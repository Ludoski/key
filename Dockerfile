# First stage: complete build environment
FROM maven:3.9.4-amazoncorretto-21 AS builder

# add pom.xml and source code
ADD ./pom.xml pom.xml
ADD ./src src/

# package jar
RUN mvn clean package -DskipTests

# Second stage: minimal runtime environment
FROM amazoncorretto:21.0.0-alpine

# Add curl for healthcheck
RUN apk --no-cache add curl

# copy jar from the first stage
COPY --from=builder target/key-0.0.1-SNAPSHOT.jar application.jar

EXPOSE 2549

CMD ["java", "-jar", "application.jar"]