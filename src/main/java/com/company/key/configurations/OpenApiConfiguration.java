package com.company.key.configurations;

import com.company.key.constants.ApiConstants;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Open API configuration
 */
@Configuration
public class OpenApiConfiguration {

  public static final String AUTH_TAG = "Auth";
  public static final String USER_TAG = "User";
  public static final String ROLE_TAG = "Role";
  public static final String UTIL_TAG = "Util";

  @Bean
  GroupedOpenApi allApis() {
    return GroupedOpenApi.builder().group("All")
            .pathsToMatch("/**").build();
  }

  @Bean
  GroupedOpenApi authApis() {
    return GroupedOpenApi.builder().group("Auth")
            .pathsToMatch(ApiConstants.V1_BASIC_AUTH_PATH + "/**").build();
  }

  @Bean
  GroupedOpenApi userApis() {
    return GroupedOpenApi.builder().group("User v1")
            .pathsToMatch(ApiConstants.V1_BASIC_USER_PATH + "/**").build();
  }

  @Bean
  GroupedOpenApi roleApis() {
    return GroupedOpenApi.builder().group("Role v1")
            .pathsToMatch(ApiConstants.V1_BASIC_ROLE_PATH + "/**").build();
  }

  @Bean
  GroupedOpenApi utilApis() {
    return GroupedOpenApi.builder().group("Util")
            .pathsToMatch(ApiConstants.BASIC_UTIL_PATH + "/**").build();
  }

  @Bean
  public OpenAPI customOpenAPI(@Value("${app.name}") String appName,
                               @Value("${app.version}") String appVersion,
                               @Value("${app.description}") String appDescription,
                               @Value("${app.tos.url}") String appTosUrl,
                               @Value("${app.licence.name}") String appLicenceName,
                               @Value("${app.licence.url}") String appLicenceUrl) {
    return new OpenAPI()
            .info(new Info()
                    .title(appName)
                    .version(appVersion)
                    .description(appDescription)
                    .termsOfService(appTosUrl)
                    .license(new License().name(appLicenceName).url(appLicenceUrl)));
  }

}
