package com.company.key.configurations;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate6.Hibernate6Module;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;

@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
@EnableCaching
public class RedisConfiguration {

  @Value("${spring.redis.host}")
  private String host;

  @Value("${spring.redis.port}")
  private int port;

  @Value("${spring.redis.password}")
  private String password;

  @Value("${spring.redis.database}")
  private int database;

  @Value("${spring.redis.ttl-minutes}")
  private int ttl;

  @Bean
  public ReactiveRedisConnectionFactory redisFirstConnectionFactory() {
    var redisConf = new RedisStandaloneConfiguration();
    redisConf.setHostName(host);
    redisConf.setPort(port);
    redisConf.setPassword(password);
    redisConf.setDatabase(database);
    return new LettuceConnectionFactory(redisConf,
            LettuceClientConfiguration
                    .builder()
                    .commandTimeout(Duration.ofSeconds(10))
                    .build());
  }

  @Bean
  public ReactiveStringRedisTemplate redisTemplate(ReactiveRedisConnectionFactory cf) {
    return new ReactiveStringRedisTemplate(cf);
  }

  @Bean
  public CacheManager cacheManager(RedisConnectionFactory factory) {
    ObjectMapper objectMapper = new ObjectMapper();
    ObjectMapper customMapper = objectMapper.copy()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .registerModule(
                    new Hibernate6Module()
                            .enable(Hibernate6Module.Feature.FORCE_LAZY_LOADING)
                            .enable(Hibernate6Module.Feature.REPLACE_PERSISTENT_COLLECTIONS)
            )
            .activateDefaultTyping(
                    objectMapper.getPolymorphicTypeValidator(),
                    ObjectMapper.DefaultTyping.EVERYTHING,
                    JsonTypeInfo.As.PROPERTY
            );

    RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration
            .defaultCacheConfig()
            .serializeKeysWith(RedisSerializationContext.SerializationPair
                    .fromSerializer(new StringRedisSerializer()))
            .serializeValuesWith(RedisSerializationContext.SerializationPair
                    .fromSerializer(new GenericJackson2JsonRedisSerializer(customMapper)))
            .disableCachingNullValues()
            .entryTtl(Duration.ofMinutes(ttl));
    return RedisCacheManager.builder(factory)
            .cacheDefaults(redisCacheConfiguration)
            .build();
  }

}
