package com.company.key.repository;

import com.company.key.models.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * User repository
 */
@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

  /**
   * Find user by email
   * @param email       User email
   * @return            Optional user
   */
  Optional<User> findByEmail(String email);

  /**
   * Find user by activation token
   * @param activationToken       User activation token
   * @return                      Optional user
   */
  Optional<User> findByActivationToken(UUID activationToken);

  /**
   * Find user by password reset token
   * @param passwordResetToken        Password reset token
   * @return                          Optional user
   */
  Optional<User> findByPasswordResetToken(UUID passwordResetToken);

  /**
   * Count filterable users by email or username or first name or last name
   * containing filter string
   * @param filter1           Filter
   * @param filter2           Filter
   * @param filter3           Filter
   * @param filter4           Filter
   * @return                  Number of users
   */
  long countAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(String filter1, String filter2, String filter3, String filter4);

  /**
   * Filter users by email or username or first name or last name
   * containing filter string
   * @param filter1           Filter
   * @param filter2           Filter
   * @param filter3           Filter
   * @param filter4           Filter
   * @return                  Page of users
   */
  Page<User> findAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(String filter1, String filter2, String filter3, String filter4, Pageable pageable);

}
