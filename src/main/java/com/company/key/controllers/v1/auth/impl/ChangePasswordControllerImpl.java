package com.company.key.controllers.v1.auth.impl;

import com.company.key.controllers.v1.auth.ChangePasswordController;
import com.company.key.dtos.auth.ChangePasswordDTO;
import com.company.key.services.auth.ChangePasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see ChangePasswordController
 */
@RestController
public class ChangePasswordControllerImpl implements ChangePasswordController {

  @Autowired
  private ChangePasswordService changePasswordService;

  /**
   * @see ChangePasswordController#changePassword(ChangePasswordDTO)
   */
  @Override
  public void changePassword(ChangePasswordDTO changePasswordDTO) {
    changePasswordService.changePassword(changePasswordDTO);
  }

}
