package com.company.key.controllers.v1.user.impl;

import com.company.key.constants.CacheConstants;
import com.company.key.controllers.v1.user.ToggleUserActiveController;
import com.company.key.dtos.user.UserDTO;
import com.company.key.models.user.User;
import com.company.key.services.user.ToggleUserActiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see ToggleUserActiveController
 */
@RestController
public class ToggleUserActiveControllerImpl implements ToggleUserActiveController {

  @Autowired
  private ToggleUserActiveService toggleUserActiveService;

  /**
   * @see ToggleUserActiveController#toggleUserActive(UUID)
   */
  @CachePut(value = CacheConstants.USERS, key = "#id")
  @Override
  public UserDTO toggleUserActive(UUID id) {
    User user = toggleUserActiveService.toggleUserActive(id);
    return new UserDTO(user);
  }

}
