package com.company.key.controllers.v1.user.impl;

import com.company.key.constants.CacheConstants;
import com.company.key.controllers.v1.user.FilterUsersController;
import com.company.key.dtos.user.FilterUsersDTO;
import com.company.key.dtos.user.FilteredUsersDTO;
import com.company.key.dtos.user.UserDTO;
import com.company.key.models.user.User;
import com.company.key.services.user.CountFilterableUsersService;
import com.company.key.services.user.GetPageOfFilteredUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @see FilterUsersController
 */
@RestController
public class FilterUsersControllerImpl implements FilterUsersController {

  @Autowired
  private CountFilterableUsersService countFilterableUsersService;

  @Autowired
  private GetPageOfFilteredUsersService getPageOfFilteredUsersService;

  /**
   * @see FilterUsersController#filterUsers(FilterUsersDTO)
   */
  @Cacheable(value = CacheConstants.FILTERED_USERS, key = "#filterUsersDTO")
  @Transactional
  @Override
  public FilteredUsersDTO filterUsers(FilterUsersDTO filterUsersDTO) {
    // Count filterable users
    long numberOfFilterableUsers = countFilterableUsersService.countFilterableUsers(filterUsersDTO);

    // Get page of users
    List<User> users = getPageOfFilteredUsersService.filterUsers(filterUsersDTO);

    // Transform db models to DTO models
    List<UserDTO> usersDTOS = users.stream()
            .map(UserDTO::new)
            .toList();

    return FilteredUsersDTO.builder()
            .total(numberOfFilterableUsers)
            .users(usersDTOS)
            .build();
  }

}
