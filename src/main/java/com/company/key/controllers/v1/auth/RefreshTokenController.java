package com.company.key.controllers.v1.auth;

import com.company.key.configurations.OpenApiConfiguration;
import com.company.key.constants.ApiConstants;
import com.company.key.constants.RoleConstants;
import com.company.key.dtos.auth.TokensDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Refresh user token controller
 */
public interface RefreshTokenController {

  /**
   * Refresh user token
   * @param httpServletRequest        Request
   * @return                          Access token
   */
  @Tag(name = OpenApiConfiguration.AUTH_TAG)
  @Operation(summary = "Refresh user token")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "New access token generated"),
          @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be GET", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_USER)
  @GetMapping(path = ApiConstants.V1_REFRESH_TOKEN, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  TokensDTO refreshToken(HttpServletRequest httpServletRequest);

}
