package com.company.key.controllers.v1.role.impl;

import com.company.key.constants.CacheConstants;
import com.company.key.controllers.v1.role.AddRoleToUserController;
import com.company.key.dtos.user.UserDTO;
import com.company.key.models.user.User;
import com.company.key.services.role.AddRoleToUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Controller;

import java.util.UUID;

/**
 * @see AddRoleToUserController
 */
@Controller
public class AddRoleToUserControllerImpl implements AddRoleToUserController {

  @Autowired
  private AddRoleToUserService addRoleToUserService;

  /**
   * @see AddRoleToUserController#addRoleToUser(UUID, UUID)
   */
  @CachePut(value = CacheConstants.USERS, key = "#userId")
  @Override
  public UserDTO addRoleToUser(UUID roleId, UUID userId) {
    User user = addRoleToUserService.addRoleToUser(roleId, userId);
    return new UserDTO(user);
  }

}
