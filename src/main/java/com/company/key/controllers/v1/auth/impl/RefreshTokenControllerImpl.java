package com.company.key.controllers.v1.auth.impl;

import com.company.key.controllers.v1.auth.RefreshTokenController;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.services.auth.RefreshTokenService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see RefreshTokenController
 */
@RestController
public class RefreshTokenControllerImpl implements RefreshTokenController {

  @Autowired
  private RefreshTokenService refreshTokenService;

  /**
   * @see RefreshTokenController#refreshToken(HttpServletRequest)
   */
  @Override
  public TokensDTO refreshToken(HttpServletRequest httpServletRequest) {
    return refreshTokenService.refreshToken(httpServletRequest);
  }

}
