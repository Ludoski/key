package com.company.key.controllers.v1.user.impl;

import com.company.key.controllers.v1.user.MeController;
import com.company.key.dtos.user.UserDTO;
import com.company.key.models.user.User;
import com.company.key.services.user.MeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see MeController
 */
@RestController
public class MeControllerImpl implements MeController {

  @Autowired
  private MeService meService;

  /**
   * @see MeController#me()
   */
  @Override
  public UserDTO me() {
    User user = meService.me();
    return new UserDTO(user);
  }

}
