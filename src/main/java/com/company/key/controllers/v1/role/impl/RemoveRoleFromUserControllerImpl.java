package com.company.key.controllers.v1.role.impl;

import com.company.key.constants.CacheConstants;
import com.company.key.controllers.v1.role.RemoveRoleFromUserController;
import com.company.key.dtos.user.UserDTO;
import com.company.key.models.user.User;
import com.company.key.services.role.RemoveRoleFromUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Controller;

import java.util.UUID;

/**
 * @see RemoveRoleFromUserController
 */
@Controller
public class RemoveRoleFromUserControllerImpl implements RemoveRoleFromUserController {

  @Autowired
  private RemoveRoleFromUserService removeRoleFromUserService;

  /**
   * @see RemoveRoleFromUserController#removeRoleFromUser(UUID, UUID)
   */
  @CachePut(value = CacheConstants.USERS, key = "#userId")
  @Override
  public UserDTO removeRoleFromUser(UUID roleId, UUID userId) {
    User user = removeRoleFromUserService.removeRoleFromUser(roleId, userId);
    return new UserDTO(user);
  }

}
