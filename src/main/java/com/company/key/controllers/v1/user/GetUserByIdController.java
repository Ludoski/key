package com.company.key.controllers.v1.user;

import com.company.key.configurations.OpenApiConfiguration;
import com.company.key.constants.ApiConstants;
import com.company.key.constants.RoleConstants;
import com.company.key.dtos.user.UserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

/**
 * Getting user information by id controller
 */
public interface GetUserByIdController {

  /**
   * Get user by id
   * @param id        User id
   * @return          User information
   */
  @Tag(name = OpenApiConfiguration.USER_TAG)
  @Operation(summary = "Get user by id")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "User found"),
          @ApiResponse(responseCode = "400", description = "User id must be UUID", content = @Content),
          @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
          @ApiResponse(responseCode = "403", description = "Not admin or not getting self", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be GET", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_USER)
  @GetMapping(path = ApiConstants.V1_GET_USER_BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  UserDTO getUserById(@PathVariable UUID id);

}
