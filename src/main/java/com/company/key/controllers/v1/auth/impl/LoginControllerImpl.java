package com.company.key.controllers.v1.auth.impl;

import com.company.key.controllers.v1.auth.LoginController;
import com.company.key.dtos.auth.LoginDTO;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.services.auth.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see LoginController
 */
@RestController
public class LoginControllerImpl implements LoginController {

  @Autowired
  private LoginService loginService;

  /**
   * @see LoginController#login(LoginDTO)
   */
  @Override
  public TokensDTO login(LoginDTO loginDTO) {
    return loginService.login(loginDTO.getEmail(), loginDTO.getPassword());
  }

}
