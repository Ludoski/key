package com.company.key.controllers.v1.auth.impl;

import com.company.key.controllers.v1.auth.ConfirmEmailController;
import com.company.key.services.auth.ConfirmEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see ConfirmEmailController
 */
@RestController
public class ConfirmEmailControllerImpl implements ConfirmEmailController {

  @Autowired
  private ConfirmEmailService confirmEmailService;

  /**
   * @see ConfirmEmailController#confirmEmail(UUID)
   */
  @Override
  public void confirmEmail(UUID activationToken) {
    confirmEmailService.confirmEmail(activationToken);
  }

}
