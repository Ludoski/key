package com.company.key.controllers.v1.auth.impl;

import com.company.key.controllers.v1.auth.RegisterController;
import com.company.key.dtos.auth.RegisterDTO;
import com.company.key.dtos.user.UserDTO;
import com.company.key.models.user.User;
import com.company.key.services.auth.RegisterUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see RegisterController
 */
@RestController
public class RegisterControllerImpl implements RegisterController {

  @Autowired
  private RegisterUserService registerUserService;

  /**
   * @see RegisterController#register(RegisterDTO)
   */
  @Override
  public UserDTO register(RegisterDTO registerDTO) {
    User user = registerUserService.registerUser(new User(registerDTO));
    return new UserDTO(user);
  }

}
