package com.company.key.controllers.v1.auth.impl;

import com.company.key.controllers.v1.auth.PasswordResetController;
import com.company.key.dtos.auth.PasswordResetFinishDTO;
import com.company.key.dtos.auth.PasswordResetInitiateDTO;
import com.company.key.services.auth.PasswordResetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see PasswordResetController
 */
@RestController
public class PasswordResetControllerImpl implements PasswordResetController {

  @Autowired
  private PasswordResetService passwordResetService;

  /**
   * @see PasswordResetController#passwordResetInitiate(PasswordResetInitiateDTO)
   */
  @Override
  public void passwordResetInitiate(PasswordResetInitiateDTO passwordResetInitiateDTO) {
    passwordResetService.passwordResetInitiate(passwordResetInitiateDTO);
  }

  /**
   * @see PasswordResetController#passwordResetFinish(PasswordResetFinishDTO)
   */
  @Override
  public void passwordResetFinish(PasswordResetFinishDTO passwordResetFinishDTO) {
    passwordResetService.passwordResetFinish(passwordResetFinishDTO);
  }

}
