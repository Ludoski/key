package com.company.key.controllers.v1.auth;

import com.company.key.configurations.OpenApiConfiguration;
import com.company.key.constants.ApiConstants;
import com.company.key.dtos.auth.RegisterDTO;
import com.company.key.dtos.user.UserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Create user controller
 */
public interface RegisterController {

  /**
   * Create user
   * @param registerDTO       User information
   * @return                  Registered user
   */
  @Tag(name = OpenApiConfiguration.AUTH_TAG)
  @Operation(summary = "Register user")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "User registered"),
          @ApiResponse(responseCode = "400", description = "Request body, email, username or password is invalid", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be POST", content = @Content),
          @ApiResponse(responseCode = "415", description = "Content type should be application/json", content = @Content),
          @ApiResponse(responseCode = "500", description = "Unable to send registration email", content = @Content)
  })
  @PostMapping(path = ApiConstants.V1_REGISTER,
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  UserDTO register(@RequestBody @Validated RegisterDTO registerDTO);

}
