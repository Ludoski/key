package com.company.key.controllers.v1.user.impl;

import com.company.key.constants.CacheConstants;
import com.company.key.controllers.v1.user.GetUserByIdController;
import com.company.key.dtos.user.UserDTO;
import com.company.key.models.user.User;
import com.company.key.services.user.GetUserByIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see GetUserByIdController
 */
@RestController
public class GetUserByIdControllerImpl implements GetUserByIdController {

  @Autowired
  private GetUserByIdService getUserByIdService;

  /**
   * @see GetUserByIdController#getUserById(UUID)
   */
  @Cacheable(value= CacheConstants.USERS, key="#id")
  @Override
  public UserDTO getUserById(UUID id) {
    User user = getUserByIdService.getUserById(id);
    return new UserDTO(user);
  }

}
