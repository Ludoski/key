package com.company.key.controllers.v1.user;

import com.company.key.configurations.OpenApiConfiguration;
import com.company.key.constants.ApiConstants;
import com.company.key.constants.RoleConstants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

/**
 * Delete user by id controller
 */
public interface DeleteUserController {

  /**
   * Delete user by id
   * @param id        User id
   */
  @Tag(name = OpenApiConfiguration.USER_TAG)
  @Operation(summary = "Delete user")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "User deleted"),
          @ApiResponse(responseCode = "400", description = "User id must be UUID", content = @Content),
          @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
          @ApiResponse(responseCode = "403", description = "Not admin and not deleting self", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be DELETE", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_USER)
  @DeleteMapping(path = ApiConstants.V1_DELETE_USER)
  @ResponseBody
  void deleteUser(@PathVariable UUID id);

}
