package com.company.key.controllers.v1.user;

import com.company.key.configurations.OpenApiConfiguration;
import com.company.key.constants.ApiConstants;
import com.company.key.constants.RoleConstants;
import com.company.key.dtos.user.UpdateUserDTO;
import com.company.key.dtos.user.UserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

/**
 * Update user information by id controller
 */
public interface UpdateUserController {

  /**
   * Update user by id
   * @param id                    User id
   * @param updateUserDTO         Update information
   * @return                      Updated user information
   */
  @Tag(name = OpenApiConfiguration.USER_TAG)
  @Operation(summary = "Update user")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "User updated"),
          @ApiResponse(responseCode = "400", description = "User id must be UUID, request body or email is invalid", content = @Content),
          @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
          @ApiResponse(responseCode = "403", description = "Not admin or not updating self", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be PATCH", content = @Content),
          @ApiResponse(responseCode = "415", description = "Content type should be application/json", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_USER)
  @PatchMapping(path = ApiConstants.V1_UPDATE_USER, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  UserDTO updateUser(@PathVariable UUID id, @RequestBody @Validated UpdateUserDTO updateUserDTO);

}
