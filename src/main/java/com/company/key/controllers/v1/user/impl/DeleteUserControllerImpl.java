package com.company.key.controllers.v1.user.impl;

import com.company.key.constants.CacheConstants;
import com.company.key.controllers.v1.user.DeleteUserController;
import com.company.key.services.user.DeleteUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see DeleteUserController
 */
@RestController
public class DeleteUserControllerImpl implements DeleteUserController {

  @Autowired
  private DeleteUserService deleteUserService;

  /**
   * @see DeleteUserController#deleteUser(UUID)
   */
  @CacheEvict(value= CacheConstants.USERS, key="#id")
  @Override
  public void deleteUser(UUID id) {
    deleteUserService.deleteUser(id);
  }

}
