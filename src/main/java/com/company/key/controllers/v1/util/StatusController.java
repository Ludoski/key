package com.company.key.controllers.v1.util;

import com.company.key.configurations.OpenApiConfiguration;
import com.company.key.constants.ApiConstants;
import com.company.key.dtos.util.StatusDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Application status and health check
 */
public interface StatusController {

  /**
   * Get app running time
   * @return      App status
   */
  @Tag(name = OpenApiConfiguration.UTIL_TAG)
  @Operation(summary = "Get application status")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Status"),
          @ApiResponse(responseCode = "405", description = "HTTP method must be GET", content = @Content),
  })
  @GetMapping(path = ApiConstants.STATUS, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  StatusDTO status();

}
