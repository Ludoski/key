package com.company.key.controllers.v1.user.impl;

import com.company.key.constants.CacheConstants;
import com.company.key.controllers.v1.user.UpdateUserController;
import com.company.key.dtos.user.UpdateUserDTO;
import com.company.key.dtos.user.UserDTO;
import com.company.key.models.user.User;
import com.company.key.services.user.UpdateUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @see UpdateUserController
 */
@RestController
public class UpdateUserControllerImpl implements UpdateUserController {

  @Autowired
  private UpdateUserService updateUserService;

  /**
   * @see UpdateUserController#updateUser(UUID, UpdateUserDTO)
   */
  @CachePut(value= CacheConstants.USERS, key="#id")
  @Override
  public UserDTO updateUser(UUID id, UpdateUserDTO updateUserDTO) {
    User user = updateUserService.updateUser(id, new User(updateUserDTO));
    return new UserDTO(user);
  }

}
