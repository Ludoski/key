package com.company.key.controllers.v1.role;

import com.company.key.configurations.OpenApiConfiguration;
import com.company.key.constants.ApiConstants;
import com.company.key.constants.RoleConstants;
import com.company.key.dtos.user.UserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

/**
 * Remove role from user controller
 */
public interface RemoveRoleFromUserController {

  /**
   * Remove role from user
   * @param roleId      Role id
   * @param userId      User id
   * @return            User
   */
  @Tag(name = OpenApiConfiguration.ROLE_TAG)
  @Operation(summary = "Remove role from user")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Removed role from user"),
          @ApiResponse(responseCode = "400", description = "Role id must be UUID", content = @Content),
          @ApiResponse(responseCode = "400", description = "User id must be UUID", content = @Content),
          @ApiResponse(responseCode = "400", description = "Removal of role user is forbidden", content = @Content),
          @ApiResponse(responseCode = "403", description = "Not admin", content = @Content),
          @ApiResponse(responseCode = "404", description = "Role not found", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "404", description = "User does not have role", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be DELETE", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_ADMIN)
  @DeleteMapping(path = ApiConstants.V1_REMOVE_ROLE_FROM_USER,
          produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  UserDTO removeRoleFromUser(@PathVariable("role-id") UUID roleId, @PathVariable("user-id") UUID userId);

}
