package com.company.key.controllers.v1.user;

import com.company.key.configurations.OpenApiConfiguration;
import com.company.key.constants.ApiConstants;
import com.company.key.constants.RoleConstants;
import com.company.key.dtos.user.UserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Getting personal information controller
 */
public interface MeController {

  /**
   * Get personal information
   * @return          User information
   */
  @Tag(name = OpenApiConfiguration.USER_TAG)
  @Operation(summary = "Get self")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "User found"),
          @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
          @ApiResponse(responseCode = "404", description = "User not found", content = @Content),
          @ApiResponse(responseCode = "405", description = "HTTP method must be GET", content = @Content)
  })
  @PreAuthorize(RoleConstants.HAS_ROLE_USER)
  @GetMapping(path = ApiConstants.V1_ME, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  UserDTO me();

}
