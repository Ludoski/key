package com.company.key.models.auth;

import com.company.key.models.role.Role;
import com.company.key.models.user.User;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

/**
 * User Principal used throughout the application to represent
 * authenticated user. Contains all information about current user.
 */
@ToString
public class UserPrincipal implements UserDetails {

  private final User user;

  public UserPrincipal(User user) {
    this.user = user;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    Set<Role> roles = user.getRoles();
    List<SimpleGrantedAuthority> authorities = new ArrayList<>();

    for (Role role: roles) {
      authorities.add(new SimpleGrantedAuthority(role.getSlug().toString()));
    }

    return authorities;
  }

  public UUID getId() {
    return this.user.getId();
  }

  @Override
  public String getPassword() {
    return this.user.getPassword();
  }

  @Override
  public String getUsername() {
    return this.user.getEmail();
  }

  @Override
  public boolean isAccountNonExpired() {
    return this.user.getActive();
  }

  @Override
  public boolean isAccountNonLocked() {
    return this.user.getActive();
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return this.user.getActive();
  }

  @Override
  public boolean isEnabled() {
    return this.user.getActive();
  }

}