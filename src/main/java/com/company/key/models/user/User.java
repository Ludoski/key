package com.company.key.models.user;

import com.company.key.dtos.auth.RegisterDTO;
import com.company.key.dtos.user.UpdateUserDTO;
import com.company.key.enums.UserRole;
import com.company.key.models.role.Role;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * User database model
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "users")
public class User implements Serializable {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private UUID id;

  @Column(name = "email")
  private String email;

  @Column(name = "password")
  private String password;

  @Column(name = "username")
  private String username;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "roles")
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
          name = "user_role",
          joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
          inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
  )
  private Set<Role> roles;

  @Column(name = "active")
  private Boolean active;

  @Column(name = "created_at")
  private Timestamp createdAt;

  @Column(name = "last_login")
  private Timestamp lastLogin;

  @Column(name = "deactivated_at")
  private Timestamp deactivatedAt;

  @Column(name = "activation_token")
  private UUID activationToken;

  @Column(name = "password_reset_token")
  private UUID passwordResetToken;

  public User(RegisterDTO registerDTO) {
    this.email = registerDTO.getEmail();
    this.password = registerDTO.getPassword();
    this.username = registerDTO.getUsername();
    this.firstName = registerDTO.getFirstName();
    this.lastName = registerDTO.getLastName();
  }

  public User(UpdateUserDTO updateUserDTO) {
    this.email = updateUserDTO.getEmail();
    this.username = updateUserDTO.getUsername();
    this.firstName = updateUserDTO.getFirstName();
    this.lastName = updateUserDTO.getLastName();
  }

  public boolean haveRole(UserRole userRole) {
    return !this.getRoles().stream()
            .filter(role -> role.getSlug().equals(userRole))
            .collect(Collectors.toSet())
            .isEmpty();
  }

}
