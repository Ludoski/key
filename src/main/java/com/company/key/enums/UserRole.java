package com.company.key.enums;

/**
 * User roles enum
 */
public enum UserRole {
  ROLE_ADMIN,
  ROLE_USER
}
