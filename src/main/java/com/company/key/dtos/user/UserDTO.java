package com.company.key.dtos.user;

import com.company.key.constants.MiscConstants;
import com.company.key.dtos.role.RoleDTO;
import com.company.key.models.role.Role;
import com.company.key.models.user.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * User DTO
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

  @JsonProperty
  private UUID id;

  @JsonProperty
  private String email;

  @JsonProperty
  private String username;

  @JsonProperty("first_name")
  private String firstName;

  @JsonProperty("last_name")
  private String lastName;

  @JsonProperty
  private Set<RoleDTO> roles;

  @JsonProperty
  private Boolean active;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = MiscConstants.DATE_TIME_FORMAT)
  @JsonProperty("created_at")
  private Timestamp createdAt;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = MiscConstants.DATE_TIME_FORMAT)
  @JsonProperty("last_login")
  private Timestamp lastLogin;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = MiscConstants.DATE_TIME_FORMAT)
  @JsonProperty("deactivated_at")
  private Timestamp deactivatedAt;

  public UserDTO(User user) {
    this.id = user.getId();
    this.email = user.getEmail();
    this.username = user.getUsername();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.roles = mapRoles(user.getRoles());
    this.active = user.getActive();
    this.createdAt = user.getCreatedAt();
    this.lastLogin = user.getLastLogin();
    this.deactivatedAt = user.getDeactivatedAt();
  }

  private Set<RoleDTO> mapRoles(Set<Role> roles) {
    return roles.stream()
            .map(role -> RoleDTO.builder()
                    .id(role.getId())
                    .slug(role.getSlug())
                    .name(role.getName())
                    .description(role.getDescription())
                    .build())
            .collect(Collectors.toSet());
  }

}
