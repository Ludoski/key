package com.company.key.dtos.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.*;

/**
 * Filter users DTO
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FilterUsersDTO {

  @JsonProperty
  private String filter;

  @NotNull
  @PositiveOrZero
  private Integer page;

  @NotNull
  @PositiveOrZero
  private Integer size;

}
