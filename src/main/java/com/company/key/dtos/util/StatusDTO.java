package com.company.key.dtos.util;

import com.company.key.constants.MiscConstants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.sql.Timestamp;

@Getter
@Builder
public class StatusDTO {

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = MiscConstants.DATE_TIME_FORMAT)
  @JsonProperty("started_at")
  private Timestamp startedAt;

  @JsonProperty("runtime_in_ms")
  private long runtimeInMs;

}
