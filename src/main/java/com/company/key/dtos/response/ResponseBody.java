package com.company.key.dtos.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Map;

@NoArgsConstructor
public class ResponseBody {

  @JsonIgnore
  private final ObjectMapper objectMapper = new ObjectMapper();

  @JsonProperty
  private HttpStatus status;

  @JsonProperty
  private Object detail;

  public ResponseBody(HttpStatus status, Object detail) {
    this.status = status;
    this.detail = detail;
  }

  public HttpStatus getStatus() {
    return status;
  }

  public Object getDetail() {
    return detail;
  }

  public Map<String, Object> toMap() {
    return objectMapper.convertValue(this, new TypeReference<>() {});
  }

}
