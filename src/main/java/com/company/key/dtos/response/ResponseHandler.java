package com.company.key.dtos.response;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Map;

/**
 * Handler for wrapping up the response message when status is OK
 */
@RestControllerAdvice
public class ResponseHandler implements ResponseBodyAdvice<Object> {

  @Override
  public boolean supports(MethodParameter returnType, @Nullable Class<? extends HttpMessageConverter<?>> converterType) {
    String className = returnType.getContainingClass().toString();
    return className.contains("Controller") && !className.contains("BasicErrorController");
  }

  @Override
  public Map<String, Object> beforeBodyWrite(Object body,
                                             @Nullable MethodParameter returnType,
                                             @Nullable MediaType selectedContentType,
                                             @Nullable Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                             @Nullable ServerHttpRequest request,
                                             @Nullable ServerHttpResponse response) {

    return new ResponseBody(HttpStatus.OK, body).toMap();
  }

}
