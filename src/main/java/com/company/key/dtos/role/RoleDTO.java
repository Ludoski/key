package com.company.key.dtos.role;

import com.company.key.enums.UserRole;
import com.company.key.models.role.Role;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.UUID;

/**
 * Role DTO
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleDTO {

  @JsonProperty
  private UUID id;

  @JsonProperty
  private UserRole slug;

  @JsonProperty
  private String name;

  @JsonProperty
  private String description;

  public RoleDTO(Role role) {
    this.id = role.getId();
    this.slug = role.getSlug();
    this.name = role.getName();
    this.description = role.getDescription();
  }

}
