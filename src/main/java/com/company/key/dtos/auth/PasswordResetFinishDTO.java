package com.company.key.dtos.auth;

import com.company.key.constants.MiscConstants;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.UUID;

/**
 * Reset password new password DTO
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PasswordResetFinishDTO {

  @NotNull
  @JsonProperty
  private UUID token;

  @Size(min = MiscConstants.PASSWORD_MIN_LENGTH)
  @NotBlank
  @NotNull
  @JsonProperty
  private String password;

}
