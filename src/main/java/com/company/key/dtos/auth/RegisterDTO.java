package com.company.key.dtos.auth;

import com.company.key.constants.MiscConstants;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

/**
 * Create user DTO
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisterDTO {

  @Email
  @NotBlank
  @NotNull
  @JsonProperty
  private String email;

  @Size(min = MiscConstants.PASSWORD_MIN_LENGTH)
  @NotBlank
  @NotNull
  @JsonProperty
  private String password;

  @NotBlank
  @NotNull
  @JsonProperty
  private String username;

  @JsonProperty("first_name")
  private String firstName;

  @JsonProperty("last_name")
  private String lastName;

}
