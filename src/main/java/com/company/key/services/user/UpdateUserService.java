package com.company.key.services.user;

import com.company.key.models.user.User;

import java.util.UUID;

/**
 * Update user information by id service
 */
public interface UpdateUserService {

  /**
   * Update user by id
   * @param id              User id
   * @param user            Update model
   * @return                Updated user model
   */
  User updateUser(UUID id, User user);

}
