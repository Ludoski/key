package com.company.key.services.user;

import com.company.key.models.user.User;

import java.util.UUID;

/**
 * Service for toggling user active
 */
public interface ToggleUserActiveService {

  /**
   * Toggle user active
   * @param id          User id
   * @return            User model
   */
  User toggleUserActive(UUID id);

}
