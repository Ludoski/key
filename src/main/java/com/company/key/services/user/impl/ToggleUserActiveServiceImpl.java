package com.company.key.services.user.impl;

import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.ToggleUserActiveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.UUID;

/**
 * @see ToggleUserActiveService
 */
@Slf4j
@Service
public class ToggleUserActiveServiceImpl implements ToggleUserActiveService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see ToggleUserActiveService#toggleUserActive(UUID)
   */
  @Transactional
  @Override
  public User toggleUserActive(UUID id) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to toggle active for user with id ({}).", currentUserId, id);

    // Check if user exists
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isEmpty()) {
      log.error("User ({}) not found.", id);
      throw new UserNotFoundException();
    }

    User user = optionalUser.get();

    // Enable/disable
    user.setActive(!user.getActive());

    if (Boolean.TRUE.equals(user.getActive())) {
      // If user is active delete "deactivated at"
      user.setDeactivatedAt(null);
      log.info("User ({}) activated user with id ({}).", currentUserId, id);
    } else {
      // Set user deactivated time in UTC
      Timestamp now = Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime());
      user.setDeactivatedAt(now);
      log.info("User ({}) deactivated user with id ({}).", currentUserId, id);
    }

    return user;
  }

}
