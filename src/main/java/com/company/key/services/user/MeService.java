package com.company.key.services.user;

import com.company.key.models.user.User;

/**
 * Getting personal user information about service
 */
public interface MeService {

  /**
   * Get personal information
   * @return          User model
   */
  User me();

}
