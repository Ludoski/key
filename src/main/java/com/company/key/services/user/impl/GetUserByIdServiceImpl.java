package com.company.key.services.user.impl;

import com.company.key.enums.UserRole;
import com.company.key.exceptions.auth.ForbiddenException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.GetUserByIdService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see GetUserByIdService
 */
@Slf4j
@Service
public class GetUserByIdServiceImpl implements GetUserByIdService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see GetUserByIdService#getUserById(UUID)
   */
  @Transactional
  @Override
  public User getUserById(UUID id) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to fetch user with id ({}).", currentUserId, id);

    // Return if user is not admin and want to get other
    if (!currentUserService.hasAuthority(UserRole.ROLE_ADMIN) &&
            !currentUserId.equals(id)) {
      log.error("User ({}) is forbidden to fetch user ({})", currentUserId, id);
      throw new ForbiddenException();
    }

    // Get user if exists
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isEmpty()) {
      log.error("User with id ({}) not found.", id);
      throw new UserNotFoundException();
    }

    log.info("User ({}) successfully fetched user with id ({}).", currentUserId, id);

    return optionalUser.get();
  }

}
