package com.company.key.services.user;

import com.company.key.models.user.User;

/**
 * Getting user information by email service
 */
public interface GetUserByEmailService {

  /**
   * Get user by id
   * @param email         User email
   * @return              User model
   */
  User getUserByEmail(String email);

}
