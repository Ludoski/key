package com.company.key.services.user.impl;

import com.company.key.enums.UserRole;
import com.company.key.exceptions.auth.ForbiddenException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.DeleteUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see DeleteUserService
 */
@Slf4j
@Service
public class DeleteUserServiceImpl implements DeleteUserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see DeleteUserService#deleteUser(UUID)
   */
  @Transactional
  @Override
  public void deleteUser(UUID id) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to delete user with id ({}). ", currentUserId, id);

    // Return if user is not admin and want to delete other
    if (!currentUserService.hasAuthority(UserRole.ROLE_ADMIN) &&
            !currentUserId.equals(id)) {
      log.error("User ({}) is forbidden to delete user ({})", currentUserId, id);
      throw new ForbiddenException();
    }

    // Get user if exists
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isEmpty()) {
      log.error("User with id ({}) not found.", id);
      throw new UserNotFoundException();
    }
    User user = optionalUser.get();

    // Delete user
    userRepository.delete(user);

    log.info("User ({}) successfully deleted user with id ({}).", currentUserId, id);
  }

}
