package com.company.key.services.user.impl;

import com.company.key.dtos.user.FilterUsersDTO;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.CountFilterableUsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * @see CountFilterableUsersService
 */
@Slf4j
@Service
public class CountFilterableUsersServiceImpl implements CountFilterableUsersService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see CountFilterableUsersService#countFilterableUsers(FilterUsersDTO)
   */
  @Transactional
  @Override
  public long countFilterableUsers(FilterUsersDTO filterUsersDTO) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    String filter = filterUsersDTO.getFilter();
    if (filter == null || filter.trim().isBlank()) {
      filter = "";
    }
    int page = filterUsersDTO.getPage();
    int size = filterUsersDTO.getSize();

    log.info("User ({}) is trying to count users by filter ({}), page ({}) and size ({}).",
            currentUserId, filter, page, size);

    // Count all filterable users from database
   long numberOfFilterableUsers = userRepository.countAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(filter, filter, filter, filter);

    log.info("User ({}) successfully counted users by filter ({}), page ({}) and size ({}).",
            currentUserId, filter, page, size);

    return numberOfFilterableUsers;
  }

}
