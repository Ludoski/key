package com.company.key.services.user;


import com.company.key.dtos.user.FilterUsersDTO;

/**
 * Count filterable users service
 */
public interface CountFilterableUsersService {

  /**
   * Count filterable users
   * @param filterUsersDTO              Parameters
   * @return                            Number of users
   */
  long countFilterableUsers(FilterUsersDTO filterUsersDTO);

}
