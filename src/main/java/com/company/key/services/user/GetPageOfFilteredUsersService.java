package com.company.key.services.user;

import com.company.key.dtos.user.FilterUsersDTO;
import com.company.key.models.user.User;

import java.util.List;

/**
 * Filter users service
 */
public interface GetPageOfFilteredUsersService {

  /**
   * Filter users
   * @param filterUsersDTO              Parameters
   * @return                            List of all users
   */
  List<User> filterUsers(FilterUsersDTO filterUsersDTO);

}
