package com.company.key.services.user.impl;

import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.GetUserByEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see GetUserByEmailService
 */
@Slf4j
@Service
public class GetUserByEmailServiceImpl implements GetUserByEmailService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see GetUserByEmailService#getUserByEmail(String)
   */
  @Transactional
  @Override
  public User getUserByEmail(String email) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to fetch user with email ({}).", currentUserId, email);

    // Get user if exists
    Optional<User> optionalUser = userRepository.findByEmail(email);
    if (optionalUser.isEmpty()) {
      log.error("User with email ({}) not found.", email);
      throw new UserNotFoundException();
    }

    log.info("User ({}) successfully fetched user with email ({}).", currentUserId, email);

    return optionalUser.get();
  }

}
