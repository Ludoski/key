package com.company.key.services.user;

import java.util.UUID;

/**
 * Delete user by id service
 */
public interface DeleteUserService {

  /**
   * Delete user by id
   * @param id        User id
   */
  void deleteUser(UUID id);

}
