package com.company.key.services.user.impl;

import com.company.key.enums.UserRole;
import com.company.key.exceptions.auth.ForbiddenException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.exceptions.validation.InvalidEmailException;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.UpdateUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see UpdateUserService
 */
@Slf4j
@Service
public class UpdateUserServiceImpl implements UpdateUserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see UpdateUserService#updateUser(UUID, User)
   */
  @Transactional
  @Override
  public User updateUser(UUID id, User user) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to update user with id ({})", currentUserId, id);

    // Return if user is not admin and want to update someone else
    if (!currentUserService.hasAuthority(UserRole.ROLE_ADMIN) && !currentUserId.equals(id)) {
      log.error("User ({}) is forbidden to update user ({})", currentUserId, id);
      throw new ForbiddenException();
    }

    // Check if email is valid
    if (user.getEmail() != null && user.getEmail().isBlank()) {
      log.error("Email must not be blank. Aborting user ({}) update.", id);
      throw new InvalidEmailException();
    }

    // Get user if exists
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isEmpty()) {
      log.error("User with id ({}) not found.", id);
      throw new UserNotFoundException();
    }

    User existingUser = optionalUser.get();

    // Update what is needed
    if (user.getEmail() != null) {
      existingUser.setEmail(user.getEmail());
    }

    if (user.getUsername() != null) {
      existingUser.setUsername(user.getUsername());
    }

    if (user.getFirstName() != null) {
      existingUser.setFirstName(user.getFirstName());
    }

    if (user.getLastName() != null) {
      existingUser.setLastName(user.getLastName());
    }

    log.info("User ({}) successfully updated user with id ({}).", currentUserId, id);

    return existingUser;
  }

}
