package com.company.key.services.user;

import com.company.key.models.user.User;

import java.util.UUID;

/**
 * Getting user information by id service
 */
public interface GetUserByIdService {

  /**
   * Get user by id
   * @param id        User id
   * @return          User model
   */
  User getUserById(UUID id);

}
