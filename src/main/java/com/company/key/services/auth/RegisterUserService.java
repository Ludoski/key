package com.company.key.services.auth;

import com.company.key.models.user.User;

/**
 * Register user service
 */
public interface RegisterUserService {

  /**
   * Register user
   * @param user       User information
   * @return           Registered user
   */
  User registerUser(User user);

}
