package com.company.key.services.auth;

import com.company.key.dtos.auth.TokensDTO;
import jakarta.servlet.http.HttpServletRequest;

/**
 * Refresh user token service
 */
public interface RefreshTokenService {

  /**
   * Refresh user token
   * @param httpServletRequest        Request
   * @return                          Access token
   */
  TokensDTO refreshToken(HttpServletRequest httpServletRequest);

}
