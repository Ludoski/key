package com.company.key.services.auth;

import com.company.key.dtos.auth.PasswordResetFinishDTO;
import com.company.key.dtos.auth.PasswordResetInitiateDTO;

/**
 * Reset password service
 */
public interface PasswordResetService {

  /**
   * Initiate password reset
   * @param passwordResetInitiateDTO       Password reset initiate payload
   */
  void passwordResetInitiate(PasswordResetInitiateDTO passwordResetInitiateDTO);

  /**
   * Finish password reset
   * @param passwordResetFinishDTO         Password reset finish payload
   */
  void passwordResetFinish(PasswordResetFinishDTO passwordResetFinishDTO);

}
