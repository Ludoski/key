package com.company.key.services.auth.impl;

import com.company.key.dtos.auth.TokensDTO;
import com.company.key.exceptions.auth.UnauthorizedException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.services.auth.JwtTokenService;
import com.company.key.services.auth.RefreshTokenService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

/**
 * @see RefreshTokenService
 */
@Slf4j
@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {

  @Autowired
  private JwtTokenService jwtTokenService;

  @Autowired
  private UserDetailsService userDetailsService;

  /**
   * @see RefreshTokenService#refreshToken(HttpServletRequest)
   */
  @Transactional
  @Override
  public TokensDTO refreshToken(HttpServletRequest httpServletRequest) {
    Optional<String> token = Optional.ofNullable(httpServletRequest.getHeader(AUTHORIZATION)); //Authorization: Bearer TOKEN
    if (token.isEmpty()) {
      log.error("Token is empty.");
      throw new UnauthorizedException();
    }

    String username = jwtTokenService.getClaims(token.get()).get("username");
    if (username == null) {
      log.error("No username in token.");
      throw new UnauthorizedException();
    }

    log.info("Trying to refresh access token for user ({})", username);
    // Get user if exists
    UserDetails userDetails;
    try {
      userDetails = userDetailsService.loadUserByUsername(username);
    } catch (UsernameNotFoundException e) {
      log.error("User ({}) not found.", username, e);
      throw new UserNotFoundException();
    }

    String accessToken = jwtTokenService.createJwtAccessToken(userDetails);

    log.info("Successfully refreshed token for user ({}). Returning access token.", username);
    return new TokensDTO(accessToken);
  }

}
