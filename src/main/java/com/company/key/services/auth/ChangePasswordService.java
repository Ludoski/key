package com.company.key.services.auth;

import com.company.key.dtos.auth.ChangePasswordDTO;

/**
 * Change user password
 */
public interface ChangePasswordService {

  /**
   * Change user password
   * @param changePasswordDTO              User passwords
   */
  void changePassword(ChangePasswordDTO changePasswordDTO);

}
