package com.company.key.services.auth.impl;

import com.company.key.enums.UserRole;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;

/**
 * @see CurrentUserService
 */
@Slf4j
@Service
public class CurrentUserServiceImpl implements CurrentUserService {

  @Autowired
  private UserRepository userRepository;

  /**
   * @see CurrentUserService#getId()
   */
  @Override
  public UUID getId() {
    Jwt jwt = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    return UUID.fromString(jwt.getClaims().get("id").toString());
  }

  /**
   * @see CurrentUserService#hasAuthority(UserRole)
   */
  @Override
  public boolean hasAuthority(UserRole userRole) {
    Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
    return authorities.contains(new SimpleGrantedAuthority(userRole.toString()));
  }

}
