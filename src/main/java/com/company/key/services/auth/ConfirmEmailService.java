package com.company.key.services.auth;

import java.util.UUID;

/**
 * Email confirmation service
 */
public interface ConfirmEmailService {

  /**
   * Confirm user email
   * @param activationToken         Token generated at registration
   */
  void confirmEmail(UUID activationToken);

}
