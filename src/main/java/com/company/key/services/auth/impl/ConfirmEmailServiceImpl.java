package com.company.key.services.auth.impl;

import com.company.key.exceptions.validation.InvalidUserActivationTokenException;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.ConfirmEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see ConfirmEmailService
 */
@Slf4j
@Service
public class ConfirmEmailServiceImpl implements ConfirmEmailService {

  @Autowired
  private UserRepository userRepository;

  /**
   * @see ConfirmEmailService#confirmEmail(UUID)
   */
  @Override
  @Transactional
  public void confirmEmail(UUID activationToken) {
    log.info("Trying to activate user with activation token ({}).", activationToken);

    // Get user if exists
    Optional<User> optionalUser = userRepository.findByActivationToken(activationToken);
    if (optionalUser.isEmpty()) {
      log.error("User with activation token ({}) not found.", activationToken);
      throw new InvalidUserActivationTokenException();
    }

    User user = optionalUser.get();

    // If user is already active just return
    if (Boolean.TRUE.equals(user.getActive()) && user.getDeactivatedAt() == null) {
      return;
    }

    // Set user active
    user.setActive(true);

    // Remove deactivated at
    user.setDeactivatedAt(null);

    // Remove activation token
    user.setActivationToken(null);

    log.info("Successfully activated user ({}).", user.getId());
  }

}
