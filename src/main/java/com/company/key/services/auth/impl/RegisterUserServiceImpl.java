package com.company.key.services.auth.impl;

import com.company.key.enums.UserRole;
import com.company.key.exceptions.role.RoleNotFoundException;
import com.company.key.exceptions.user.UserAlreadyExistsException;
import com.company.key.models.role.Role;
import com.company.key.models.user.User;
import com.company.key.repository.RoleRepository;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.RegisterUserService;
import com.company.key.services.email.UserActivationEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @see RegisterUserService
 */
@Slf4j
@Service
public class RegisterUserServiceImpl implements RegisterUserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private UserActivationEmailService userActivationEmailService;

  /**
   * @see RegisterUserService#registerUser(User)
   */
  @Transactional
  @Override
  public User registerUser(User user) {
    log.info("Trying to create user with email ({})", user.getEmail());

    // Check if user already exists
    Optional<User> optionalUser = userRepository.findByEmail(user.getEmail());
    if (optionalUser.isPresent()) {
      log.error("User with email ({}) already exists. User creation aborted.", user.getEmail());
      throw new UserAlreadyExistsException();
    }

    //try { // TODO Uncomment to enable email activation

      // Encode user password
      user.setPassword(passwordEncoder.encode(user.getPassword()));

      // Set default user role
      UserRole userRole = UserRole.ROLE_USER;
      Optional<Role> optionalRole = roleRepository.findBySlug(userRole);
      if (optionalRole.isEmpty()) {
        log.error("Role with slug ({}) do not exists. User creation aborted.", userRole);
        throw new RoleNotFoundException();
      }
      Set<Role> roles = new HashSet<>();
      roles.add(optionalRole.get());
      user.setRoles(roles);

      // Set user inactive until email is confirmed
      user.setActive(true); // TODO Set to false after email confirmation is configured

      // Get current time in UTC
      Timestamp now = Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime());

      // Set user deactivated time
      user.setDeactivatedAt(now);

      // Set user creation time
      user.setCreatedAt(now);

      // Create activation token
      user.setActivationToken(UUID.randomUUID());

      // Save user
      userRepository.save(user);

      // Send user activation email
      log.info("Sending activation email to ({}).", user.getEmail());
      /*userActivationEmailService.sendUserActivationEmail(user.getEmail(), user.getActivationToken()); // TODO Uncomment to enable email activation
    } catch (MessagingException | MailAuthenticationException e) {
      log.error("Unable to send user activation email for user ({}).", user.getId(), e);
      userRepository.delete(user);
      throw new UnableToSendEmailException();
    }*/

    log.info("Successfully created user ({}).", user.getId());

    return user;
  }

}
