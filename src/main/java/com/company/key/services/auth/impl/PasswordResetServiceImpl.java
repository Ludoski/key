package com.company.key.services.auth.impl;

import com.company.key.dtos.auth.PasswordResetFinishDTO;
import com.company.key.dtos.auth.PasswordResetInitiateDTO;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.PasswordResetService;
import com.company.key.services.email.PasswordResetEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see PasswordResetService
 */
@Slf4j
@Service
public class PasswordResetServiceImpl implements PasswordResetService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordResetEmailService passwordResetEmailService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  /**
   * @see PasswordResetService#passwordResetInitiate(PasswordResetInitiateDTO)
   */
  @Transactional
  @Override
  public void passwordResetInitiate(PasswordResetInitiateDTO passwordResetInitiateDTO) {
    String email = passwordResetInitiateDTO.getEmail();

    log.info("Trying to initiate password reset for email ({})).", email);

    // Get user if exists
    Optional<User> optionalUser = userRepository.findByEmail(email);
    if (optionalUser.isEmpty()) {
      log.error("User with email ({}) not found.", email);
      throw new UserNotFoundException();
    }

    User user = optionalUser.get();

    //try { // TODO Uncomment to enable email activation

      // Generate reset token
      UUID passwordResetToken = UUID.randomUUID();

      // Set password reset token
      user.setPasswordResetToken(passwordResetToken);

      // Send password reset email
      log.info("Sending password reset email to ({}).", user.getEmail());
      /*passwordResetEmailService.sendPasswordResetEmail(email, passwordResetToken);  // TODO Uncomment to enable email activation
    } catch (MessagingException | MailAuthenticationException e) {
      log.error("Unable to send password reset email for user ({}).", user.getId(), e);
      throw new UnableToSendEmailException();
    }*/

    log.info("Successfully initiated password reset for user ({}).", user.getId());
  }

  /**
   * @see PasswordResetService#passwordResetFinish(PasswordResetFinishDTO)
   */
  @Transactional
  @Override
  public void passwordResetFinish(PasswordResetFinishDTO passwordResetFinishDTO) {
    UUID token = passwordResetFinishDTO.getToken();

    log.info("Trying to finish password reset with password reset token ({})).", token);

    // Get user if exists
    Optional<User> optionalUser = userRepository.findByPasswordResetToken(token);
    if (optionalUser.isEmpty()) {
      log.error("User with password reset token ({}) not found.", token);
      throw new UserNotFoundException();
    }

    User user = optionalUser.get();

    // Encode user password
    String password = passwordResetFinishDTO.getPassword();
    user.setPassword(passwordEncoder.encode(password));

    // Reset password reset token
    user.setPasswordResetToken(null);

    log.info("Successfully finished password reset for user ({}).", user.getId());
  }

}
