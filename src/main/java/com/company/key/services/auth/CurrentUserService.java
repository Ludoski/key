package com.company.key.services.auth;

import com.company.key.enums.UserRole;

import java.util.UUID;

/**
 * Service for currently logged user
 */
public interface CurrentUserService {

  /**
   * Get currently logged user id
   * @return        Currently logged user id
   */
  UUID getId();

  /**
   * Check if currently logged user have this authority
   * @param userRole        Some user role
   * @return                True or false
   */
  boolean hasAuthority(UserRole userRole);

}
