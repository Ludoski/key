package com.company.key.services.auth;

import com.company.key.dtos.auth.TokensDTO;

/**
 * User login service
 */
public interface LoginService {

  /**
   * User login
   * @param email           Email
   * @param password        Password
   * @return                Access and refresh tokens
   */
  TokensDTO login(String email, String password);

}
