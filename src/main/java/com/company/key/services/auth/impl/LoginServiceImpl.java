package com.company.key.services.auth.impl;

import com.company.key.dtos.auth.TokensDTO;
import com.company.key.exceptions.user.UserNotActiveException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.exceptions.validation.InvalidPasswordException;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.JwtTokenService;
import com.company.key.services.auth.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Optional;

/**
 * @see LoginService
 */
@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

  @Autowired
  private UserDetailsService userDetailsService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private JwtTokenService jwtTokenService;

  /**
   * @see LoginService#login(String, String)
   */
  @Transactional
  @Override
  public TokensDTO login(String email, String password) {
    log.info("Trying to login user ({})", email);

    UserDetails userDetails;
    try {
      userDetails = userDetailsService.loadUserByUsername(email);
    } catch (UsernameNotFoundException e) {
      log.error("User ({}) not found.", email, e);
      throw new UserNotFoundException();
    }

    if (!passwordEncoder.matches(password, userDetails.getPassword())) {
      log.error("User ({}) is not authorized. Invalid password.", email);
      throw new InvalidPasswordException();
    }

    // Get user if exists
    if (!userDetails.isEnabled()) {
      log.error("User with email ({}) is not active.", email);
      throw new UserNotActiveException();
    }

    // Get user if exists
    Optional<User> optionalUser = userRepository.findByEmail(email);
    if (optionalUser.isEmpty()) {
      log.error("User with email ({}) not found.", email);
      throw new UserNotFoundException();
    }

    User user = optionalUser.get();

    // Get current time in UTC
    Timestamp now = Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime());

    // Set last login to now
    user.setLastLogin(now);

    String accessToken = jwtTokenService.createJwtAccessToken(userDetails);
    String refreshToken = jwtTokenService.createJwtRefreshToken(userDetails);
    log.info("User ({}) successfully logged in. Returning tokens.", email);

    return new TokensDTO(accessToken, refreshToken);
  }
  
}
