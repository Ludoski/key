package com.company.key.services.auth.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.company.key.constants.SecurityConstants;
import com.company.key.exceptions.auth.InvalidTokenException;
import com.company.key.exceptions.auth.UnauthorizedException;
import com.company.key.models.auth.UserPrincipal;
import com.company.key.services.auth.JwtTokenService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @see JwtTokenService
 */
@Slf4j
@Service
public class JwtTokenServiceImpl implements JwtTokenService {

  @Value("${spring.security.jwt.secret}")
  private String jwtSecretKey;

  @Value("${spring.security.jwt.access-token-ttl-minutes}")
  private int accessTokenTtlMinutes;

  @Value("${spring.security.jwt.refresh-token-ttl-minutes}")
  private int refreshTokenTtlMinutes;

  /**
   * @see JwtTokenService#createJwtAccessToken(UserDetails)
   * @param userDetails          User
   */
  @Override
  public String createJwtAccessToken(UserDetails userDetails) {

    // Create expiration date
    Calendar expiresAt = Calendar.getInstance();
    expiresAt.add(Calendar.MINUTE, accessTokenTtlMinutes);

    // Create builder
    JWTCreator.Builder jwtBuilder = JWT.create().withSubject(userDetails.getUsername());

    // Add claims
    Map<String, String> claims = createClaims((UserPrincipal) userDetails);
    claims.forEach(jwtBuilder::withClaim);

    // Add expiredAt and etc
    return jwtBuilder
            .withNotBefore(Calendar.getInstance().getTime())
            .withExpiresAt(expiresAt.getTime())
            .sign(Algorithm.HMAC256(jwtSecretKey));
  }

  /**
   * @see JwtTokenService#createJwtRefreshToken(UserDetails)
   */
  @Override
  public String createJwtRefreshToken(UserDetails userDetails) {

    // Create expiration date
    Calendar expiresAt = Calendar.getInstance();
    expiresAt.add(Calendar.MINUTE, refreshTokenTtlMinutes);

    // Create builder
    JWTCreator.Builder jwtBuilder = JWT.create().withSubject(userDetails.getUsername());

    // Add claims
    Map<String, String> claims = createClaims((UserPrincipal) userDetails);
    claims.forEach(jwtBuilder::withClaim);

    // Add expiredAt and etc
    return jwtBuilder
            .withNotBefore(Calendar.getInstance().getTime())
            .withExpiresAt(expiresAt.getTime())
            .sign(Algorithm.HMAC256(jwtSecretKey));
  }

  /**
   * @see JwtTokenService#verifyToken(String)
   */
  @Override
  public void verifyToken(String token) {
    if (token == null) {
      throw new InvalidTokenException();
    }

    // Remove "Bearer " from token
    String trimmedToken = StringUtils.removeStart(token, "Bearer").trim();

    try {
      Algorithm algorithm = Algorithm.HMAC256(jwtSecretKey);
      JWTVerifier verifier = JWT.require(algorithm).build(); //Reusable verifier instance
      verifier.verify(trimmedToken);
    } catch (JWTVerificationException e) {
      throw new InvalidTokenException();
    }
  }

  /**
   * @see JwtTokenService#getClaims(String)
   */
  @Override
  public Map<String, String> getClaims(String token) {
    if (token == null) {
      throw new InvalidTokenException();
    }

    // Remove "Bearer " from token
    String trimmedToken = StringUtils.removeStart(token, "Bearer").trim();

    DecodedJWT jwt;
    Map<String, String> result = new HashMap<>();
    try {
      // Verify token
      Algorithm algorithm = Algorithm.HMAC256(jwtSecretKey);
      JWTVerifier verifier = JWT.require(algorithm).build(); //Reusable verifier instance
      jwt = verifier.verify(trimmedToken);

      // Get claims
      Map<String, Claim> claims = jwt.getClaims();

      // Map claims
      claims.forEach((key, value) -> result.put(key, value.asString()));
    } catch (JWTVerificationException | IllegalArgumentException exception){
      //Invalid signature/claims
      log.error("Invalid token.");
      throw new UnauthorizedException();
    }
    // Return mapped claims
    return result;
  }

  /**
   * Create token claims
   * @param userPrincipal       User principal
   * @return                    Claims
   */
  private Map<String, String> createClaims(UserPrincipal userPrincipal) {
    Map<String, String> claims = new HashMap<>();

    // Add id
    claims.put("id", userPrincipal.getId().toString());

    // Add username
    claims.put("username", userPrincipal.getUsername());

    // Add user roles
    String authorities = userPrincipal.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(" "));
    claims.put(SecurityConstants.AUTHORITIES_CLAIM_NAME, authorities);

    return claims;
  }

}
