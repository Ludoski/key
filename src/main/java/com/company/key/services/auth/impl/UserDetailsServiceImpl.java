package com.company.key.services.auth.impl;

import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.models.auth.UserPrincipal;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * User Details Service that handles retrieval logic of users
 * trying to authenticate against the application.
 */
@NoArgsConstructor
@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  /**
   * Retrieves user from data source and create a UserPrincipal
   * with data or throws an exception.
   *
   * @param username Username by which to search for a user
   * @return User Principal object used once user is authenticated
   * @throws UsernameNotFoundException If user does not exist in data source
   */
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<User> user = userRepository.findByEmail(username);

    if (user.isEmpty() || Boolean.FALSE.equals(user.get().getActive())) {
      throw new UserNotFoundException();
    }

    return new UserPrincipal(user.get());
  }
}