package com.company.key.services.auth.impl;

import com.company.key.dtos.auth.ChangePasswordDTO;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.exceptions.validation.InvalidPasswordException;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.ChangePasswordService;
import com.company.key.services.auth.CurrentUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

/**
 * @see ChangePasswordService
 */
@Slf4j
@Service
public class ChangePasswordServiceImpl implements ChangePasswordService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  /**
   * @see ChangePasswordService#changePassword(ChangePasswordDTO)
   */
  @Transactional
  @Override
  public void changePassword(ChangePasswordDTO changePasswordDTO) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to change password", currentUserId);

    // Get user if exists
    Optional<User> optionalUser = userRepository.findById(currentUserId);
    if (optionalUser.isEmpty()) {
      log.error("User with id ({}) not found.", currentUserId);
      throw new UserNotFoundException();
    }

    User user = optionalUser.get();

    // Check if old password is valid
    if (!passwordEncoder.matches(changePasswordDTO.getOldPassword(), user.getPassword())) {
      log.error("User's old password does not match with user's actual password.");
      throw new InvalidPasswordException();
    }

    // Encode new password
    String encodedNewPassword = passwordEncoder.encode(changePasswordDTO.getNewPassword());
    user.setPassword(encodedNewPassword);

    log.info("User ({}) successfully changed password.", currentUserId);
  }

}
