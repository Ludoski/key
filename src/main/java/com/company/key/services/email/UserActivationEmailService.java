package com.company.key.services.email;

import jakarta.mail.MessagingException;

import java.util.UUID;

/**
 * User activation email service
 */
public interface UserActivationEmailService {

  /**
   * Send user activation email
   * @param email               User email address
   * @param activationToken     User activation token
   */
  void sendUserActivationEmail(String email, UUID activationToken) throws MessagingException;

}
