package com.company.key.services.email.impl;

import com.company.key.models.email.Mail;
import com.company.key.services.email.UserActivationEmailService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @see UserActivationEmailService
 */
@Service
public class UserActivationEmailServiceImpl implements UserActivationEmailService {

  @Value("${app.name}")
  private String appName;

  @Value("${spring.mail.username}")
  private String from;

  @Value("${spring.mail.activation-url}")
  private String activationUrl;

  @Autowired
  private JavaMailSender emailSender;

  @Autowired
  private SpringTemplateEngine templateEngine;

  /**
   * @see UserActivationEmailService#sendUserActivationEmail(String, UUID)
   */
  public void sendUserActivationEmail(String email, UUID activationToken) throws MessagingException {
    Map<String, Object> properties = new HashMap<>();
    properties.put("appName", appName);
    properties.put("activationUrl", activationUrl + "?token=" + activationToken);

    Mail mail = Mail.builder()
            .from(from)
            .to(email)
            .htmlTemplate(new Mail.HtmlTemplate("user-activation-email", properties))
            .subject(appName + " user activation")
            .build();

    MimeMessage message = emailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message,
            MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
            StandardCharsets.UTF_8.name());

    String html = getHtmlContent(mail);

    helper.setTo(mail.getTo());
    helper.setFrom(mail.getFrom());
    helper.setSubject(mail.getSubject());
    helper.setText(html, true);

    emailSender.send(message);
  }

  private String getHtmlContent(Mail mail) {
    Context context = new Context();
    context.setVariables(mail.getHtmlTemplate().getProps());
    return templateEngine.process(mail.getHtmlTemplate().getTemplate(), context);
  }

}
