package com.company.key.services.role.impl;

import com.company.key.enums.UserRole;
import com.company.key.exceptions.role.RemovalOfRoleUserForbiddenException;
import com.company.key.exceptions.role.RoleNotFoundException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.models.role.Role;
import com.company.key.models.user.User;
import com.company.key.repository.RoleRepository;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.role.RemoveRoleFromUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @see RemoveRoleFromUserService
 */
@Slf4j
@Service
public class RemoveRoleFromUserServiceImpl implements RemoveRoleFromUserService {

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see RemoveRoleFromUserService#removeRoleFromUser(UUID, UUID)
   */
  @Override
  public User removeRoleFromUser(UUID roleId, UUID userId) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to remove role ({}) from user ({}).", currentUserId, roleId, userId);

    Optional<Role> optionalRole = roleRepository.findById(roleId);

    if (optionalRole.isEmpty()) {
        log.error("Role with id ({}) does not exist.", roleId);
        throw new RoleNotFoundException();
    }

    Role role = optionalRole.get();

    if (role.getSlug().equals(UserRole.ROLE_USER)) {
      log.error("Removal of ({}) is forbidden.", UserRole.ROLE_USER);
      throw new RemovalOfRoleUserForbiddenException();
    }

    Optional<User> optionalUser = userRepository.findById(userId);

    if (optionalUser.isEmpty()) {
      log.error("User with id ({}) does not exist.", roleId);
      throw new UserNotFoundException();
    }

    User user = optionalUser.get();

    if (!user.haveRole(role.getSlug())) {
      log.error("User ({}) does not have role ({}).", userId, roleId);
      throw new RoleNotFoundException();
    }

    // Remove role from user
    Set<Role> userRoles = new HashSet<>(user.getRoles());
    userRoles.remove(role);
    user.setRoles(userRoles);

    userRepository.save(user);

    log.info("Removed role ({}) from user ({}).", roleId, userId);

    return user;
  }

}
