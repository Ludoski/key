package com.company.key.services.role.impl;

import com.company.key.exceptions.role.RoleNotFoundException;
import com.company.key.exceptions.role.UserAlreadyHaveRoleException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.models.role.Role;
import com.company.key.models.user.User;
import com.company.key.repository.RoleRepository;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.role.AddRoleToUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @see AddRoleToUserService
 */
@Slf4j
@Service
public class AddRoleToUserServiceImpl implements AddRoleToUserService {

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CurrentUserService currentUserService;

  /**
   * @see AddRoleToUserService#addRoleToUser(UUID, UUID)
   */
  @Override
  public User addRoleToUser(UUID roleId, UUID userId) {
    // Get current user id
    UUID currentUserId = currentUserService.getId();

    log.info("User ({}) is trying to add role ({}) to user ({}).", currentUserId, roleId, userId);

    Optional<Role> optionalRole = roleRepository.findById(roleId);

    if (optionalRole.isEmpty()) {
        log.error("Role with id ({}) does not exist.", roleId);
        throw new RoleNotFoundException();
    }

    Optional<User> optionalUser = userRepository.findById(userId);

    if (optionalUser.isEmpty()) {
      log.error("User with id ({}) does not exist.", roleId);
      throw new UserNotFoundException();
    }

    Role role = optionalRole.get();
    User user = optionalUser.get();

    if (user.haveRole(role.getSlug())) {
      log.error("User ({}) already have role ({}).", userId, roleId);
      throw new UserAlreadyHaveRoleException();
    }

    // Add role to user
    Set<Role> userRoles = new HashSet<>(user.getRoles());
    userRoles.add(role);
    user.setRoles(userRoles);

    userRepository.save(user);

    log.info("Added role ({}) to user ({}).", roleId, userId);

    return user;
  }

}
