package com.company.key.services.role;

import com.company.key.models.user.User;

import java.util.UUID;

/**
 * Add role to user service
 */
public interface AddRoleToUserService {

  /**
   * Add role to user
   * @param roleId      Role id
   * @param userId      User id
   * @return            User
   */
  User addRoleToUser(UUID roleId, UUID userId);

}
