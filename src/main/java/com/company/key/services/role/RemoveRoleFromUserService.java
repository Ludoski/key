package com.company.key.services.role;

import com.company.key.models.user.User;

import java.util.UUID;

/**
 * Remove role from user service
 */
public interface RemoveRoleFromUserService {

  /**
   * Remove role from user
   * @param roleId      Role id
   * @param userId      User id
   * @return            User
   */
  User removeRoleFromUser(UUID roleId, UUID userId);

}
