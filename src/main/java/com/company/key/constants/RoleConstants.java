package com.company.key.constants;

public class RoleConstants {

  private RoleConstants() {
    throw new IllegalStateException("Utility class");
  }

  public static final String HAS_ROLE_ADMIN = "hasRole('ROLE_ADMIN')";
  public static final String HAS_ROLE_USER = "hasRole('ROLE_USER')";

}
