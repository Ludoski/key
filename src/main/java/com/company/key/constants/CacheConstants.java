package com.company.key.constants;

public class CacheConstants {

  private CacheConstants() {
    throw new IllegalStateException("Utility class");
  }

  public static final String USERS = "key_users";
  public static final String FILTERED_USERS = "key_filtered_users";

}
