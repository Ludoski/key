package com.company.key.constants;

public class SecurityConstants {

  private SecurityConstants() {
    throw new IllegalStateException("Utility class");
  }

  public static final String AUTHORITIES_CLAIM_NAME = "roles";

}
