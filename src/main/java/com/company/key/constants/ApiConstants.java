package com.company.key.constants;

public class ApiConstants {

  private ApiConstants() {
    throw new IllegalStateException("Utility class");
  }

  // Util
  public static final String BASIC_UTIL_PATH = "/util";
  public static final String STATUS = BASIC_UTIL_PATH + "/status";
  public static final String OPENAPI_DOCS = "/v3/api-docs/**";
  public static final String SWAGGER_UI = "/swagger-ui**/**";

    // v1
  public static final String V1_BASIC_PATH = "/v1";

  // Auth
  public static final String V1_BASIC_AUTH_PATH = V1_BASIC_PATH + "/auth";
  public static final String V1_REGISTER = V1_BASIC_AUTH_PATH + "/register";
  public static final String V1_LOGIN = V1_BASIC_AUTH_PATH + "/login";
  public static final String V1_REFRESH_TOKEN = V1_BASIC_AUTH_PATH + "/refresh";
  public static final String V1_CHANGE_PASSWORD = V1_BASIC_AUTH_PATH + "/password";
  public static final String V1_CONFIRM_EMAIL = V1_BASIC_AUTH_PATH + "/confirm-email/{activation-token}";
  public static final String V1_PASSWORD_RESET = V1_BASIC_AUTH_PATH + "/password-reset";

  // User v1
  public static final String V1_BASIC_USER_PATH = V1_BASIC_PATH + "/user";
  public static final String V1_ME = V1_BASIC_USER_PATH + "/me";
  public static final String V1_GET_USER_BY_ID = V1_BASIC_USER_PATH + "/{id}";
  public static final String V1_UPDATE_USER = V1_BASIC_USER_PATH + "/{id}";
  public static final String V1_DELETE_USER = V1_BASIC_USER_PATH + "/{id}";
  public static final String V1_TOGGLE_USER_ACTIVE = V1_BASIC_USER_PATH + "/{id}/toggle-active";
  public static final String V1_FILTER_ALL_USERS = V1_BASIC_USER_PATH + "/filter";

  // Role v1
  public static final String V1_BASIC_ROLE_PATH = V1_BASIC_PATH + "/role";
  public static final String V1_ADD_ROLE_TO_USER = V1_BASIC_ROLE_PATH + "/{role-id}/{user-id}";
  public static final String V1_REMOVE_ROLE_FROM_USER = V1_BASIC_ROLE_PATH + "/{role-id}/{user-id}";

}
