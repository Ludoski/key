package com.company.key.constants;

public class MiscConstants {

  private MiscConstants() {
    throw new IllegalStateException("Utility class");
  }

  public static final int PASSWORD_MIN_LENGTH = 8;
  public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

}
