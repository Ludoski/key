package com.company.key.exceptions.auth;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Forbidden exception
 */
public class ForbiddenException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "You are forbidden to access this resource.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.FORBIDDEN;

  public ForbiddenException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
