package com.company.key.exceptions.auth;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Invalid user authorization exception
 */
public class InvalidTokenException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Token is invalid.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.UNAUTHORIZED;

  public InvalidTokenException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
