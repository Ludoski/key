package com.company.key.exceptions.auth;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Invalid user authorization exception
 */
public class UnauthorizedException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "You are not authorized to access this resource.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.UNAUTHORIZED;

  public UnauthorizedException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
