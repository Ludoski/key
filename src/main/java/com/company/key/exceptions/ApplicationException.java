package com.company.key.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Global application custom exception
 */
public class ApplicationException extends RuntimeException {

  private final String responseMessage;
  private final HttpStatus httpStatus;

  public ApplicationException(String responseMessage, HttpStatus httpStatus) {
    super(responseMessage);
    this.responseMessage = responseMessage;
    this.httpStatus = httpStatus;
  }

  public String getResponseMessage() {
    return responseMessage;
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }
}
