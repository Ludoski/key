package com.company.key.exceptions.role;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Removal of role user is forbidden exception
 */
public class RemovalOfRoleUserForbiddenException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Removal of role user is forbidden.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public RemovalOfRoleUserForbiddenException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
