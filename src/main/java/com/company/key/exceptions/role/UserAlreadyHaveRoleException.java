package com.company.key.exceptions.role;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * User have role exception
 */
public class UserAlreadyHaveRoleException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "User already have role.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public UserAlreadyHaveRoleException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
