package com.company.key.exceptions.email;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Forbidden exception
 */
public class UnableToSendEmailException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Unable to send email.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

  public UnableToSendEmailException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
