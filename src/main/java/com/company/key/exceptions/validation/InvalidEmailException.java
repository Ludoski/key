package com.company.key.exceptions.validation;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Invalid user email exception
 */
public class InvalidEmailException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Invalid email.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public InvalidEmailException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
