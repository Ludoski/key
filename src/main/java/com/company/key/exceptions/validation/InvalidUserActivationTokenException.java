package com.company.key.exceptions.validation;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Invalid user email exception
 */
public class InvalidUserActivationTokenException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Invalid user activation token.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public InvalidUserActivationTokenException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
