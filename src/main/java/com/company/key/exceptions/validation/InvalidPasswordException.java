package com.company.key.exceptions.validation;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Invalid user password exception
 */
public class InvalidPasswordException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Invalid password.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public InvalidPasswordException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
