package com.company.key.exceptions.user;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * User not found exception
 */
public class UserNotActiveException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "User is not active.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_FOUND;

  public UserNotActiveException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
