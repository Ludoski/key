package com.company.key.exceptions.user;

import com.company.key.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * User already exists exception
 */
public class UserAlreadyExistsException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "User already exists.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public UserAlreadyExistsException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
