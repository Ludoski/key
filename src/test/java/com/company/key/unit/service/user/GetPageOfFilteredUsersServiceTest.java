package com.company.key.unit.service.user;

import com.company.key.dtos.user.FilterUsersDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.impl.GetPageOfFilteredUsersServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

class GetPageOfFilteredUsersServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private CurrentUserService currentUserService;

  @InjectMocks
  private GetPageOfFilteredUsersServiceImpl getPageOfFilteredUsersService;

  @Test
  @DisplayName("Successfully filter users")
  void filterUsersTest() {
    User user = helperService.getUser();
    String filter = "check";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();
    Page<User> userPage = new PageImpl<>(List.of(user));

    doReturn(userPage)
            .when(userRepository).findAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(any(), any(), any(), any(), any());

    List<User> result = getPageOfFilteredUsersService.filterUsers(filterUsersDTO);

    assertThat(result.get(0).getEmail()).isEqualTo(user.getEmail());
    assertThat(result).hasSize(1);
  }

  @Test
  @DisplayName("Successfully filter users, filter null")
  void filterUsersFilterNullTest() {
    User user = helperService.getUser();
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .page(page)
            .size(size)
            .build();
    Page<User> userPage = new PageImpl<>(List.of(user));

    doReturn(userPage)
            .when(userRepository).findAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(any(), any(), any(), any(), any());

    List<User> result = getPageOfFilteredUsersService.filterUsers(filterUsersDTO);

    assertThat(result.get(0).getEmail()).isEqualTo(user.getEmail());
    assertThat(result).hasSize(1);
  }

  @Test
  @DisplayName("Successfully filter users, filter empty")
  void filterUsersFilterEmptyTest() {
    User user = helperService.getUser();
    String filter = "";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();
    Page<User> userPage = new PageImpl<>(List.of(user));

    doReturn(userPage)
            .when(userRepository).findAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(any(), any(), any(), any(), any());

    List<User> result = getPageOfFilteredUsersService.filterUsers(filterUsersDTO);

    assertThat(result.get(0).getEmail()).isEqualTo(user.getEmail());
    assertThat(result).hasSize(1);
  }

  @Test
  @DisplayName("Successfully filter users, filter space")
  void filterUsersFilterSpaceTest() {
    User user = helperService.getUser();
    String filter = " ";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();
    Page<User> userPage = new PageImpl<>(List.of(user));

    doReturn(userPage)
            .when(userRepository).findAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(any(), any(), any(), any(), any());

    List<User> result = getPageOfFilteredUsersService.filterUsers(filterUsersDTO);

    assertThat(result.get(0).getEmail()).isEqualTo(user.getEmail());
    assertThat(result).hasSize(1);
  }

}
