package com.company.key.unit.service.user;

import com.company.key.dtos.user.FilterUsersDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.impl.CountFilterableUsersServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

class CountFilterableUsersServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private CurrentUserService currentUserService;

  @InjectMocks
  private CountFilterableUsersServiceImpl countFilterableUsersService;

  @Test
  @DisplayName("Successfully count filterable users")
  void countFilterableUsersTest() {
    String filter = "check";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    doReturn(1L)
            .when(userRepository).countAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(any(), any(), any(), any());

    long result = countFilterableUsersService.countFilterableUsers(filterUsersDTO);

    assertThat(result).isEqualTo(1);
  }

  @Test
  @DisplayName("Successfully count filterable users, filter null")
  void countFilterableUsersFilterNullTest() {
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .page(page)
            .size(size)
            .build();

    doReturn(1L)
            .when(userRepository).countAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(any(), any(), any(), any());

    long result = countFilterableUsersService.countFilterableUsers(filterUsersDTO);

    assertThat(result).isEqualTo(1);
  }

  @Test
  @DisplayName("Successfully count filterable users, filter empty")
  void countFilterableUsersFilterEmptyTest() {
    String filter = "";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    doReturn(1L)
            .when(userRepository).countAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(any(), any(), any(), any());

    long result = countFilterableUsersService.countFilterableUsers(filterUsersDTO);

    assertThat(result).isEqualTo(1);
  }

  @Test
  @DisplayName("Successfully count filterable users, filter space")
  void countFilterableUsersFilterSpaceTest() {
    String filter = " ";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    doReturn(1L)
            .when(userRepository).countAllByEmailContainingIgnoreCaseOrUsernameContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(any(), any(), any(), any());

    long result = countFilterableUsersService.countFilterableUsers(filterUsersDTO);

    assertThat(result).isEqualTo(1);
  }

}
