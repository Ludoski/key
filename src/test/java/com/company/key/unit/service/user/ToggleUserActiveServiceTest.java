package com.company.key.unit.service.user;

import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.impl.ToggleUserActiveServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

class ToggleUserActiveServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private CurrentUserService currentUserService;

  @InjectMocks
  private ToggleUserActiveServiceImpl toggleUserActiveService;

  @Test
  @DisplayName("Successfully toggled user active")
  void toggleUserActiveTest() {
    User user = helperService.getUser();

    doReturn(Optional.of(user))
            .when(userRepository).findById(any());

    User result = toggleUserActiveService.toggleUserActive(user.getId());

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

  @Test
  @DisplayName("User not found")
  void userNotFoundTest() {
    User user = helperService.getUser();
    UUID userId = user.getId();

    assertThatThrownBy(() -> toggleUserActiveService.toggleUserActive(userId))
            .isInstanceOf(UserNotFoundException.class);
  }

}
