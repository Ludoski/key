package com.company.key.unit.service.auth;

import com.company.key.exceptions.role.RoleNotFoundException;
import com.company.key.exceptions.user.UserAlreadyExistsException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.auth.UserPrincipal;
import com.company.key.models.role.Role;
import com.company.key.models.user.User;
import com.company.key.repository.RoleRepository;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.impl.RegisterUserServiceImpl;
import com.company.key.services.email.UserActivationEmailService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

class RegisterUserServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private RoleRepository roleRepository;

  @Mock
  private PasswordEncoder passwordEncoder;

  @Mock
  private UserActivationEmailService userActivationEmailService;

  @InjectMocks
  private RegisterUserServiceImpl registerUserService;

  @Test
  @DisplayName("Successfully registered user")
  void registerUserTest() {
    User user = helperService.getUser();
    Role userRole = helperService.getUserRole();
    Optional<Role> optionalRole = Optional.of(userRole);

    doReturn(Optional.empty())
            .when(userRepository).findByEmail(any());
    doReturn(optionalRole)
            .when(roleRepository).findBySlug(any());
    when(passwordEncoder.encode(any()))
            .thenReturn("password");

    User registeredUser = registerUserService.registerUser(user);

    assertEquals(user.getEmail(), registeredUser.getEmail());
  }

  @Test
  @DisplayName("User already exists")
  void userAlreadyExistsTest() {
    User user = helperService.getUser();
    UserPrincipal userPrincipal = new UserPrincipal(user);

    doReturn(Optional.of(userPrincipal))
            .when(userRepository).findByEmail(any());

    assertThatThrownBy(() -> registerUserService.registerUser(user))
            .isInstanceOf(UserAlreadyExistsException.class);
  }

  @Test
  @DisplayName("Role not found")
  void roleNotFoundTest() {
    User user = helperService.getUser();

    doReturn(Optional.empty())
            .when(userRepository).findByEmail(any());
    doReturn(Optional.empty())
            .when(roleRepository).findBySlug(any());

    assertThatThrownBy(() -> registerUserService.registerUser(user))
            .isInstanceOf(RoleNotFoundException.class);
  }

  // TODO Enable this when email activation is configured
  /*@Test
  @DisplayName("Email sending error")
  void emailErrorTest() throws MessagingException {
    User user = helperService.getUser();
    Role userRole = helperService.getUserRole();

    doReturn(Optional.empty())
            .when(userRepository).findByEmail(any());
    doReturn(Optional.of(userRole))
            .when(roleRepository).findBySlug(any());
    doThrow(MessagingException.class)
            .when(userActivationEmailService).sendUserActivationEmail(any(), any());

    assertThatThrownBy(() -> registerUserService.registerUser(user))
            .isInstanceOf(UnableToSendEmailException.class);
  }*/

}
