package com.company.key.unit.service.user;

import com.company.key.exceptions.auth.ForbiddenException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.impl.DeleteUserServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class DeleteUserServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private CurrentUserService currentUserService;

  @InjectMocks
  private DeleteUserServiceImpl deleteUserService;

  @Test
  @DisplayName("Successfully delete user as admin")
  void deleteUserAsAdminTest() {
    UUID id = UUID.randomUUID();
    User user = helperService.getUser();

    when(currentUserService.hasAuthority(any()))
            .thenReturn(true);
    doReturn(Optional.of(user))
            .when(userRepository).findById(any());

    deleteUserService.deleteUser(id);

    verify(userRepository, times(1)).delete(any());
  }

  @Test
  @DisplayName("Successfully delete self")
  void deleteSelfTest() {
    UUID id = UUID.randomUUID();
    User user = helperService.getUser();

    when(currentUserService.hasAuthority(any()))
            .thenReturn(false);
    when(currentUserService.getId())
            .thenReturn(id);
    doReturn(Optional.of(user))
            .when(userRepository).findById(any());

    deleteUserService.deleteUser(id);

    verify(userRepository, times(1)).delete(any());
  }

  @Test
  @DisplayName("User not found")
  void userNotFoundTest() {
    UUID userId = UUID.randomUUID();

    when(currentUserService.hasAuthority(any()))
            .thenReturn(true);

    assertThatThrownBy(() -> deleteUserService.deleteUser(userId))
            .isInstanceOf(UserNotFoundException.class);
  }

  @Test
  @DisplayName("User not admin")
  void userNotAdminTest() {
    UUID userId = UUID.randomUUID();

    when(currentUserService.getId())
            .thenReturn(UUID.randomUUID());
    when(currentUserService.hasAuthority(any()))
            .thenReturn(false);

    assertThatThrownBy(() -> deleteUserService.deleteUser(userId))
            .isInstanceOf(ForbiddenException.class);
  }

  @Test
  @DisplayName("User not admin and trying to delete other")
  void userNotAdminAndDeleteOtherTest() {
    UUID userId = UUID.randomUUID();

    when(currentUserService.getId())
            .thenReturn(UUID.randomUUID());

    assertThatThrownBy(() -> deleteUserService.deleteUser(userId))
            .isInstanceOf(ForbiddenException.class);
  }

}
