package com.company.key.unit.service.auth;

import com.company.key.dtos.auth.ChangePasswordDTO;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.exceptions.validation.InvalidPasswordException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.auth.impl.ChangePasswordServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ChangePasswordServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private CurrentUserService currentUserService;

  @Mock
  private PasswordEncoder passwordEncoder;

  @InjectMocks
  private ChangePasswordServiceImpl changePasswordService;

  @Test
  @DisplayName("Successfully changed password")
  void changedPasswordTest() {
    User user = helperService.getUser();
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(helperService.getRandomString())
            .newPassword(helperService.getRandomString())
            .build();

    when(currentUserService.getId())
            .thenReturn(user.getId());
    doReturn(Optional.of(user))
            .when(userRepository).findById(any());
    when(passwordEncoder.matches(any(), any()))
            .thenReturn(true);

    changePasswordService.changePassword(changePasswordDTO);

    verify(userRepository).findById(any());
  }

  @Test
  @DisplayName("User not found")
  void userNotFoundTest() {
    User user = helperService.getUser();
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(helperService.getRandomString())
            .newPassword(helperService.getRandomString())
            .build();

    when(currentUserService.getId())
            .thenReturn(user.getId());
    doReturn(Optional.empty())
            .when(userRepository).findById(any());

    assertThatThrownBy(() -> changePasswordService.changePassword(changePasswordDTO))
            .isInstanceOf(UserNotFoundException.class);
  }

  @Test
  @DisplayName("Passwords do not match")
  void passwordsDoNotMatchTest() {
    User user = helperService.getUser();
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(helperService.getRandomString())
            .newPassword(helperService.getRandomString())
            .build();

    when(currentUserService.getId())
            .thenReturn(user.getId());
    doReturn(Optional.of(user))
            .when(userRepository).findById(any());
    when(passwordEncoder.matches(any(), any()))
            .thenReturn(false);

    assertThatThrownBy(() -> changePasswordService.changePassword(changePasswordDTO))
            .isInstanceOf(InvalidPasswordException.class);
  }

}
