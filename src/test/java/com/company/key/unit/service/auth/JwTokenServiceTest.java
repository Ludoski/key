package com.company.key.unit.service.auth;

import com.company.key.enums.UserRole;
import com.company.key.exceptions.auth.InvalidTokenException;
import com.company.key.exceptions.auth.UnauthorizedException;
import com.company.key.models.auth.UserPrincipal;
import com.company.key.models.role.Role;
import com.company.key.models.user.User;
import com.company.key.services.auth.impl.JwtTokenServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class JwTokenServiceTest {

  private static final String mockedJwtSecretKey = "secret key";

  private UserDetails userDetails;

  @InjectMocks
  private JwtTokenServiceImpl jwtTokenService;

  @BeforeEach
  void setup() {
    ReflectionTestUtils.setField(jwtTokenService, "jwtSecretKey", mockedJwtSecretKey);

    Role role = Role.builder()
            .slug(UserRole.ROLE_USER)
            .build();

    Set<Role> roles = new HashSet<>();
    roles.add(role);

    User user = User.builder()
            .id(UUID.randomUUID())
            .username("username")
            .roles(roles)
            .build();

    userDetails = new UserPrincipal(user);
  }

  @Test
  void createJwtAccessTokenSuccessfulTest() {
    assertNotNull(jwtTokenService.createJwtAccessToken(userDetails));
  }

  @Test
  void createJwtRefreshTokenSuccessfulTest() {
    assertNotNull(jwtTokenService.createJwtRefreshToken(userDetails));
  }

  @Test
  void getClaimsInvalidTokenThrowsUnauthorizedExceptionTest() {
    String token = jwtTokenService.createJwtAccessToken(userDetails);
    // Make token invalid
    token = token.replace(".", "*");

    String finalToken = token;
    assertThatThrownBy(() -> jwtTokenService.getClaims(finalToken))
            .isInstanceOf(UnauthorizedException.class);
  }

  @Test
  void getClaimsNullTokenThrowsInvalidTokenExceptionTest() {
    assertThatThrownBy(() -> jwtTokenService.getClaims(null))
            .isInstanceOf(InvalidTokenException.class);
  }

  @Test
  void verifyTokenInvalidTest() {
    String token = jwtTokenService.createJwtAccessToken(userDetails);
    // Make token invalid
    token = token.replace(".", "*");

    String finalToken = token;
    assertThatThrownBy(() -> jwtTokenService.verifyToken(finalToken))
            .isInstanceOf(InvalidTokenException.class);
  }

  @Test
  void verifyTokenNullTokenThrowsInvalidTokenExceptionTest() {
    assertThatThrownBy(() -> jwtTokenService.getClaims(null))
            .isInstanceOf(InvalidTokenException.class);
  }

}
