package com.company.key.unit.service.user;

import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.impl.MeServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

class MeServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private CurrentUserService currentUserService;

  @InjectMocks
  private MeServiceImpl meService;

  @Test
  @DisplayName("Successfully get user")
  void success() {
    User user = helperService.getUser();

    when(currentUserService.getId())
            .thenReturn(user.getId());
    doReturn(Optional.of(user))
            .when(userRepository).findById(any());

    User result = meService.me();

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

  @Test
  @DisplayName("User not found")
  void userNotFoundTest() {
    User user = helperService.getUser();

    when(currentUserService.getId())
            .thenReturn(user.getId());

    assertThatThrownBy(() -> meService.me())
            .isInstanceOf(UserNotFoundException.class);
  }

}
