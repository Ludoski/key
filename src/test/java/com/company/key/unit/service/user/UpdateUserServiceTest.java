package com.company.key.unit.service.user;

import com.company.key.exceptions.auth.ForbiddenException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.exceptions.validation.InvalidEmailException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.user.impl.UpdateUserServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

class UpdateUserServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private CurrentUserService currentUserService;

  @InjectMocks
  private UpdateUserServiceImpl updateUserService;

  @Test
  @DisplayName("Successfully updated user as admin")
  void updateUserAsAdminTest() {
    User user = helperService.getUser();

    when(currentUserService.hasAuthority(any()))
            .thenReturn(true);
    doReturn(Optional.of(user))
            .when(userRepository).findById(any());

    User result = updateUserService.updateUser(user.getId(), user);

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

  @Test
  @DisplayName("Successfully update self")
  void updateSelfTest() {
    User user = helperService.getUser();

    when(currentUserService.hasAuthority(any()))
            .thenReturn(false);
    when(currentUserService.getId())
            .thenReturn(user.getId());
    doReturn(Optional.of(user))
            .when(userRepository).findById(any());

    User result = updateUserService.updateUser(user.getId(), user);

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

  @Test
  @DisplayName("Email empty")
  void emailTest() {
    User user = helperService.getUser();
    UUID userId = user.getId();
    user.setEmail("");

    when(currentUserService.hasAuthority(any()))
            .thenReturn(true);

    assertThatThrownBy(() -> updateUserService.updateUser(userId, user))
            .isInstanceOf(InvalidEmailException.class);
  }

  @Test
  @DisplayName("User not found")
  void userNotFoundTest() {
    User user = helperService.getUser();
    UUID userId = user.getId();

    when(currentUserService.hasAuthority(any()))
            .thenReturn(true);

    assertThatThrownBy(() -> updateUserService.updateUser(userId, user))
            .isInstanceOf(UserNotFoundException.class);
  }

  @Test
  @DisplayName("User not admin")
  void userNotAdminTest() {
    User user = helperService.getUser();
    UUID userId = UUID.randomUUID();

    when(currentUserService.getId())
            .thenReturn(UUID.randomUUID());
    when(currentUserService.hasAuthority(any()))
            .thenReturn(false);


    assertThatThrownBy(() -> updateUserService.updateUser(userId, user))
            .isInstanceOf(ForbiddenException.class);
  }

  @Test
  @DisplayName("User not admin and trying to update other")
  void userNotAdminAndUpdateOtherTest() {
    User user = helperService.getUser();
    UUID userId = UUID.randomUUID();

    when(currentUserService.hasAuthority(any()))
            .thenReturn(false);
    when(currentUserService.getId())
            .thenReturn(UUID.randomUUID());

    assertThatThrownBy(() -> updateUserService.updateUser(userId, user))
            .isInstanceOf(ForbiddenException.class);
  }

}
