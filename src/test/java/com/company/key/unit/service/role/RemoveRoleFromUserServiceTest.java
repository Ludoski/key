package com.company.key.unit.service.role;

import com.company.key.exceptions.role.RemovalOfRoleUserForbiddenException;
import com.company.key.exceptions.role.RoleNotFoundException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.role.Role;
import com.company.key.models.user.User;
import com.company.key.repository.RoleRepository;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.CurrentUserService;
import com.company.key.services.role.impl.RemoveRoleFromUserServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

class RemoveRoleFromUserServiceTest extends BaseUnitTest {

  @Mock
  private RoleRepository roleRepository;

  @Mock
  private UserRepository userRepository;

  @Mock
  private CurrentUserService currentUserService;

  @InjectMocks
  private RemoveRoleFromUserServiceImpl removeRoleFromUserService;

  @Test
  @DisplayName("Successfully removed role")
  void successTest() {
    Role role = helperService.getAdminRole();
    User user = helperService.getAdmin();

    when(currentUserService.getId())
            .thenReturn(UUID.randomUUID());
    doReturn(Optional.of(role))
            .when(roleRepository).findById(any());
    doReturn(Optional.of(user))
            .when(userRepository).findById(any());

    User modifiedUser = removeRoleFromUserService.removeRoleFromUser(role.getId(), user.getId());

    assertFalse(modifiedUser.getRoles().contains(role));
  }

  @Test
  @DisplayName("Invalid role id")
  void invalidRoleIdTest() {
    Role role = helperService.getAdminRole();
    User user = helperService.getUser();

    when(currentUserService.getId())
            .thenReturn(UUID.randomUUID());
    doReturn(Optional.empty())
            .when(roleRepository).findById(any());

    assertThatThrownBy(() -> removeRoleFromUserService.removeRoleFromUser(role.getId(), user.getId()))
            .isInstanceOf(RoleNotFoundException.class);
  }

  @Test
  @DisplayName("Role USER can not be removed")
  void roleUserCanNotBeRemovedTest() {
    Role role = helperService.getUserRole();
    User user = helperService.getUser();

    when(currentUserService.getId())
            .thenReturn(UUID.randomUUID());
    doReturn(Optional.of(role))
            .when(roleRepository).findById(any());

    assertThatThrownBy(() -> removeRoleFromUserService.removeRoleFromUser(role.getId(), user.getId()))
            .isInstanceOf(RemovalOfRoleUserForbiddenException.class);
  }

  @Test
  @DisplayName("Invalid user id")
  void invalidUserIdTest() {
    Role role = helperService.getAdminRole();
    User user = helperService.getUser();

    when(currentUserService.getId())
            .thenReturn(UUID.randomUUID());
    doReturn(Optional.of(role))
            .when(roleRepository).findById(any());
    doReturn(Optional.empty())
            .when(userRepository).findById(any());

    assertThatThrownBy(() -> removeRoleFromUserService.removeRoleFromUser(role.getId(), user.getId()))
            .isInstanceOf(UserNotFoundException.class);
  }

  @Test
  @DisplayName("User do not have role")
  void userDoNotHaveRoleTest() {
    Role role = helperService.getAdminRole();
    User user = helperService.getUser();

    when(currentUserService.getId())
            .thenReturn(UUID.randomUUID());
    doReturn(Optional.of(role))
            .when(roleRepository).findById(any());
    doReturn(Optional.of(user))
            .when(userRepository).findById(any());

    assertThatThrownBy(() -> removeRoleFromUserService.removeRoleFromUser(role.getId(), user.getId()))
            .isInstanceOf(RoleNotFoundException.class);
  }

}
