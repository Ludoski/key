package com.company.key.unit.service.auth;

import com.company.key.dtos.auth.TokensDTO;
import com.company.key.exceptions.user.UserNotActiveException;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.exceptions.validation.InvalidPasswordException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.auth.UserPrincipal;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.JwtTokenService;
import com.company.key.services.auth.impl.LoginServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class LoginServiceTest extends BaseUnitTest {

  @Mock
  private UserDetailsService userDetailsService;

  @Mock
  private UserRepository userRepository;

  @Mock
  private JwtTokenService jwtTokenService;

  @Mock
  private PasswordEncoder passwordEncoder;

  @InjectMocks
  private LoginServiceImpl loginService;

  @Test
  @DisplayName("Successful login")
  void loginTest() {
    UserPrincipal userPrincipal = new UserPrincipal(helperService.getUser());
    User user = helperService.getUser();
    String accessToken = "access_token";
    String refreshToken = "refresh_token";

    when(userDetailsService.loadUserByUsername(any()))
            .thenReturn(userPrincipal);
    when(passwordEncoder.matches(any(), any()))
            .thenReturn(true);
    when(userRepository.findByEmail(any()))
            .thenReturn(Optional.of(user));
    when(jwtTokenService.createJwtAccessToken(any()))
            .thenReturn(accessToken);
    when(jwtTokenService.createJwtRefreshToken(any()))
            .thenReturn(refreshToken);

    TokensDTO result = loginService.login(userPrincipal.getUsername(), userPrincipal.getPassword());

    assertThat(result.getAccessToken()).isEqualTo(accessToken);
    assertThat(result.getRefreshToken()).isEqualTo(refreshToken);
  }

  @Test
  @DisplayName("User not found 1")
  void userNotFound1Test() {
    when(userDetailsService.loadUserByUsername(any()))
            .thenThrow(UsernameNotFoundException.class);

    assertThatThrownBy(() -> loginService.login("username", "password"))
            .isInstanceOf(UserNotFoundException.class);
  }

  @Test
  @DisplayName("User not found 2")
  void userNotFound2Test() {
    UserPrincipal userPrincipal = new UserPrincipal(helperService.getUser());

    when(userDetailsService.loadUserByUsername(any()))
            .thenReturn(userPrincipal);
    when(passwordEncoder.matches(any(), any()))
            .thenReturn(true);

    assertThatThrownBy(() -> loginService.login("username", "password"))
            .isInstanceOf(UserNotFoundException.class);
  }

  @Test
  @DisplayName("User not active")
  void userNotActiveTest() {
    User user = helperService.getUser();
    user.setActive(false);
    UserPrincipal userPrincipal = new UserPrincipal(user);

    when(userDetailsService.loadUserByUsername(any()))
            .thenReturn(userPrincipal);
    when(passwordEncoder.matches(any(), any()))
            .thenReturn(true);

    assertThatThrownBy(() -> loginService.login("username", "password"))
            .isInstanceOf(UserNotActiveException.class);
  }

  @Test
  @DisplayName("Invalid password")
  void invalidPasswordTest() {
    UserPrincipal userPrincipal = new UserPrincipal(helperService.getUser());

    when(userDetailsService.loadUserByUsername(any()))
            .thenReturn(userPrincipal);
    when(passwordEncoder.matches(any(), any()))
            .thenReturn(false);

    assertThatThrownBy(() -> loginService.login("username", "password"))
            .isInstanceOf(InvalidPasswordException.class);
  }

}
