package com.company.key.unit.service.email;

import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.services.email.impl.PasswordResetEmailServiceImpl;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.util.ReflectionTestUtils;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class PasswordResetEmailServiceTest extends BaseUnitTest {

  @Mock
  private JavaMailSender emailSender;

  @Mock
  private SpringTemplateEngine templateEngine;

  @InjectMocks
  private PasswordResetEmailServiceImpl passwordResetEmailService;

  @Mock
  private MimeMessage message;

  @BeforeEach
  void init() {
    ReflectionTestUtils.setField(passwordResetEmailService, "appName", "App");
    ReflectionTestUtils.setField(passwordResetEmailService, "from", "mail@email.com");
    ReflectionTestUtils.setField(passwordResetEmailService, "passwordResetUrl", "http://url.com");
  }

  @Test
  @DisplayName("Successfully send password reset email")
  void success() throws MessagingException {
    String email = helperService.getRandomEmail();
    UUID passwordResetToken = UUID.randomUUID();

    when(emailSender.createMimeMessage())
            .thenReturn(message);
    when(templateEngine.process(anyString(), any()))
            .thenReturn("html");

    passwordResetEmailService.sendPasswordResetEmail(email, passwordResetToken);

    verify(emailSender, times(1)).send(message);
  }

}
