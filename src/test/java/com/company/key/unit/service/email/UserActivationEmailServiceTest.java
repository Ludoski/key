package com.company.key.unit.service.email;

import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.services.email.impl.UserActivationEmailServiceImpl;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.util.ReflectionTestUtils;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class UserActivationEmailServiceTest extends BaseUnitTest {

  @Mock
  private JavaMailSender emailSender;

  @Mock
  private SpringTemplateEngine templateEngine;

  @InjectMocks
  private UserActivationEmailServiceImpl userActivationEmailService;

  @Mock
  private MimeMessage message;

  @BeforeEach
  void init() {
    ReflectionTestUtils.setField(userActivationEmailService, "appName", "App");
    ReflectionTestUtils.setField(userActivationEmailService, "from", "mail@email.com");
    ReflectionTestUtils.setField(userActivationEmailService, "activationUrl", "http://url.com");
  }

  @Test
  @DisplayName("Successfully send password reset email")
  void success() throws MessagingException {
    String email = helperService.getRandomEmail();
    UUID activationToken = UUID.randomUUID();

    when(emailSender.createMimeMessage())
            .thenReturn(message);
    when(templateEngine.process(anyString(), any()))
            .thenReturn("html");

    userActivationEmailService.sendUserActivationEmail(email, activationToken);

    verify(emailSender, times(1)).send(message);
  }

}
