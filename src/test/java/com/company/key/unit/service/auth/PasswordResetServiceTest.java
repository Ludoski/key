package com.company.key.unit.service.auth;

import com.company.key.dtos.auth.PasswordResetFinishDTO;
import com.company.key.dtos.auth.PasswordResetInitiateDTO;
import com.company.key.exceptions.user.UserNotFoundException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.impl.PasswordResetServiceImpl;
import com.company.key.services.email.PasswordResetEmailService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

class PasswordResetServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private PasswordResetEmailService passwordResetEmailService;

  @Mock
  private PasswordEncoder passwordEncoder;

  @InjectMocks
  private PasswordResetServiceImpl passwordResetService;

  // Password reset initiate tests

  // TODO Enable this when email activation is configured
  /*@Test
  @DisplayName("Successfully initiated password reset")
  void successInitiateTest() throws MessagingException {
    User user = helperService.getUser();
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    doReturn(Optional.of(user))
            .when(userRepository).findByEmail(any());

    passwordResetService.passwordResetInitiate(passwordResetInitiateDTO);

    verify(userRepository).findByEmail(any());
    verify(passwordResetEmailService).sendPasswordResetEmail(any(), any());
  }*/

  @Test
  @DisplayName("User not found")
  void userNotFoundInitiateTest() {
    User user = helperService.getUser();
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    doReturn(Optional.empty())
            .when(userRepository).findByEmail(any());

    assertThatThrownBy(() -> passwordResetService.passwordResetInitiate(passwordResetInitiateDTO))
            .isInstanceOf(UserNotFoundException.class);
  }

  // TODO Enable this when email activation is configured
  /*@Test
  @DisplayName("Email sending error")
  void emailErrorInitiateTest() throws MessagingException {
    User user = helperService.getUser();
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    doReturn(Optional.of(user))
            .when(userRepository).findByEmail(any());
    doThrow(MessagingException.class)
            .when(passwordResetEmailService).sendPasswordResetEmail(any(), any());

    assertThatThrownBy(() -> passwordResetService.passwordResetInitiate(passwordResetInitiateDTO))
            .isInstanceOf(UnableToSendEmailException.class);
  }*/

  // Password reset finish tests

  @Test
  @DisplayName("Successfully finished password reset")
  void successFinishTest() {
    User user = helperService.getUser();
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(UUID.randomUUID())
            .password("new password")
            .build();

    doReturn(Optional.of(user))
            .when(userRepository).findByPasswordResetToken(any());

    passwordResetService.passwordResetFinish(passwordResetFinishDTO);

    verify(userRepository).findByPasswordResetToken(any());
  }

  @Test
  @DisplayName("User not found")
  void userNotFoundFinishTest() {
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(UUID.randomUUID())
            .password("new password")
            .build();

    doReturn(Optional.empty())
            .when(userRepository).findByPasswordResetToken(any());

    assertThatThrownBy(() -> passwordResetService.passwordResetFinish(passwordResetFinishDTO))
            .isInstanceOf(UserNotFoundException.class);
  }

}
