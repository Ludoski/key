package com.company.key.unit.service.auth;

import com.company.key.exceptions.validation.InvalidUserActivationTokenException;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.repository.UserRepository;
import com.company.key.services.auth.impl.ConfirmEmailServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ConfirmUserActivationEmailServiceTest extends BaseUnitTest {

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private ConfirmEmailServiceImpl confirmEmailService;

  @Test
  @DisplayName("Successfully confirmed email")
  void success1Test() {
    User user = helperService.getUser();
    user.setActive(false);
    UUID activationToken = user.getActivationToken();

    doReturn(Optional.of(user))
            .when(userRepository).findByActivationToken(any());

    confirmEmailService.confirmEmail(activationToken);

    verify(userRepository).findByActivationToken(any());
  }

  @Test
  @DisplayName("Successfully confirmed email and user is already active")
  void success2Test() {
    User user = helperService.getUser();
    user.setActive(true);
    user.setDeactivatedAt(null);
    UUID activationToken = user.getActivationToken();

    doReturn(Optional.of(user))
            .when(userRepository).findByActivationToken(any());

    confirmEmailService.confirmEmail(activationToken);

    verify(userRepository, times(0)).save(any());
  }

  @Test
  @DisplayName("Invalid activation token")
  void invalidActivationTokenTest() {
    User user = helperService.getUser();
    user.setActive(false);
    UUID activationToken = user.getActivationToken();

    doReturn(Optional.empty())
            .when(userRepository).findByActivationToken(any());

    assertThatThrownBy(() -> confirmEmailService.confirmEmail(activationToken))
            .isInstanceOf(InvalidUserActivationTokenException.class);
  }

}
