package com.company.key.unit.controller.auth;

import com.company.key.controllers.v1.auth.impl.PasswordResetControllerImpl;
import com.company.key.dtos.auth.PasswordResetFinishDTO;
import com.company.key.dtos.auth.PasswordResetInitiateDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.services.auth.PasswordResetService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

class PasswordResetControllerTest extends BaseUnitTest {

  @Mock
  private PasswordResetService passwordResetService;

  @InjectMocks
  private PasswordResetControllerImpl passwordResetController;

  @Test
  @DisplayName("Successfully initiated")
  void successInitiateTest() {
    User user = helperService.getUser();
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    passwordResetController.passwordResetInitiate(passwordResetInitiateDTO);

    verify(passwordResetService).passwordResetInitiate(any());
  }

  @Test
  @DisplayName("Successfully finished")
  void successFinishTest() {
    PasswordResetFinishDTO passwordResetFinishDTO = PasswordResetFinishDTO.builder()
            .token(UUID.randomUUID())
            .password("new password")
            .build();

    passwordResetController.passwordResetFinish(passwordResetFinishDTO);

    verify(passwordResetService).passwordResetFinish(any());
  }

}
