package com.company.key.unit.controller.user;

import com.company.key.controllers.v1.user.impl.DeleteUserControllerImpl;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.services.user.DeleteUserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class DeleteUserControllerTest extends BaseUnitTest {

  @Mock
  private DeleteUserService deleteUserService;

  @InjectMocks
  private DeleteUserControllerImpl deleteUserController;

  @Test
  @DisplayName("Successfully delete user")
  void deleteUserTest() {
    deleteUserController.deleteUser(any());

    verify(deleteUserService, times(1)).deleteUser(any());
  }

}
