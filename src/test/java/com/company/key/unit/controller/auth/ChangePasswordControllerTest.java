package com.company.key.unit.controller.auth;

import com.company.key.controllers.v1.auth.impl.ChangePasswordControllerImpl;
import com.company.key.dtos.auth.ChangePasswordDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.services.auth.ChangePasswordService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class ChangePasswordControllerTest extends BaseUnitTest {

  @Mock
  private ChangePasswordService changePasswordService;

  @InjectMocks
  private ChangePasswordControllerImpl changePasswordController;

  @Test
  @DisplayName("Successfully changed password")
  void changePasswordTest() {
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(helperService.getRandomString())
            .newPassword(helperService.getRandomString())
            .build();

    changePasswordController.changePassword(changePasswordDTO);

    verify(changePasswordService, times(1)).changePassword(changePasswordDTO);
  }

}
