package com.company.key.unit.controller.user;

import com.company.key.controllers.v1.user.impl.GetUserByIdControllerImpl;
import com.company.key.dtos.user.UserDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.services.user.GetUserByIdService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class GetUserByIdControllerTest extends BaseUnitTest {

  @Mock
  private GetUserByIdService getUserByIdService;

  @InjectMocks
  private GetUserByIdControllerImpl getUserByIdController;

  @Test
  @DisplayName("Successfully get user")
  void getUserByIdTest() {
    User user = helperService.getUser();

    when(getUserByIdService.getUserById(any()))
            .thenReturn(user);

    UserDTO result = getUserByIdController.getUserById(any());

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

}
