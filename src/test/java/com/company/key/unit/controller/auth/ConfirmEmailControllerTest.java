package com.company.key.unit.controller.auth;

import com.company.key.controllers.v1.auth.impl.ConfirmEmailControllerImpl;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.services.auth.ConfirmEmailService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

class ConfirmEmailControllerTest extends BaseUnitTest {

  @Mock
  private ConfirmEmailService confirmEmailService;

  @InjectMocks
  private ConfirmEmailControllerImpl confirmEmailController;

  @Test
  @DisplayName("Successful confirmation")
  void successTest() {
    User user = helperService.getUser();

    confirmEmailController.confirmEmail(user.getActivationToken());

    verify(confirmEmailService).confirmEmail(any());
  }

}
