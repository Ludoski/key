package com.company.key.unit.controller.user;

import com.company.key.controllers.v1.user.impl.MeControllerImpl;
import com.company.key.dtos.user.UserDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.services.user.MeService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class MeControllerTest extends BaseUnitTest {

  @Mock
  private MeService meService;

  @InjectMocks
  private MeControllerImpl meController;

  @Test
  @DisplayName("Successfully get user model for self")
  void meTest() {
    User user = helperService.getUser();

    when(meService.me())
            .thenReturn(user);

    UserDTO result = meController.me();

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

}
