package com.company.key.unit.controller.auth;

import com.company.key.controllers.v1.auth.impl.RegisterControllerImpl;
import com.company.key.dtos.auth.RegisterDTO;
import com.company.key.dtos.user.UserDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.services.auth.RegisterUserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class RegisterControllerTest extends BaseUnitTest {

  @Mock
  private RegisterUserService registerUserService;

  @InjectMocks
  private RegisterControllerImpl registerController;

  @Test
  @DisplayName("Successful register")
  void registerTest() {
    User user = helperService.getUser();

    RegisterDTO registerDTO = RegisterDTO.builder()
            .email(user.getEmail())
            .password(user.getPassword())
            .username(user.getUsername())
            .firstName("first")
            .lastName("last")
            .build();

    when(registerUserService.registerUser(any()))
            .thenReturn(user);

    UserDTO userDTO = registerController.register(registerDTO);

    assertEquals(user.getEmail(), userDTO.getEmail());
  }

}
