package com.company.key.unit.controller.user;

import com.company.key.controllers.v1.user.impl.FilterUsersControllerImpl;
import com.company.key.dtos.user.FilterUsersDTO;
import com.company.key.dtos.user.FilteredUsersDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.services.user.CountFilterableUsersService;
import com.company.key.services.user.GetPageOfFilteredUsersService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class FilterUsersControllerTest extends BaseUnitTest {

  @Mock
  private CountFilterableUsersService countFilterableUsersService;

  @Mock
  private GetPageOfFilteredUsersService getPageOfFilteredUsersService;

  @InjectMocks
  private FilterUsersControllerImpl filterAllUsersController;

  @Test
  @DisplayName("Filter all users")
  void filterAllUsersTest() {
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter("check")
            .page(0)
            .size(1)
            .build();
    List<User> users = List.of(helperService.getUser());

    when(countFilterableUsersService.countFilterableUsers(filterUsersDTO))
            .thenReturn((long) users.size());
    when(getPageOfFilteredUsersService.filterUsers(filterUsersDTO))
            .thenReturn(users);

    FilteredUsersDTO result = filterAllUsersController.filterUsers(filterUsersDTO);

    assertThat(result.getTotal()).isEqualTo(1);
    assertThat(result.getUsers().get(0).getEmail()).isEqualTo(users.get(0).getEmail());
  }

}
