package com.company.key.unit.controller.user;

import com.company.key.controllers.v1.user.impl.UpdateUserControllerImpl;
import com.company.key.dtos.user.UpdateUserDTO;
import com.company.key.dtos.user.UserDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.services.user.UpdateUserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class UpdateUserControllerTest extends BaseUnitTest {

  @Mock
  private UpdateUserService updateUserService;

  @InjectMocks
  private UpdateUserControllerImpl updateUserController;

  @Test
  @DisplayName("Successfully update user")
  void updateUserTest() {
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(helperService.getRandomEmail())
            .build();
    User user = helperService.getUser();
    user.setEmail(updateUserDTO.getEmail());

    when(updateUserService.updateUser(any(), any()))
            .thenReturn(user);

    UserDTO result = updateUserController.updateUser(user.getId(), updateUserDTO);

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

}
