package com.company.key.unit.controller.user;

import com.company.key.controllers.v1.user.impl.ToggleUserActiveControllerImpl;
import com.company.key.dtos.user.UserDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.user.User;
import com.company.key.services.user.ToggleUserActiveService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ToggleUserActiveControllerTest extends BaseUnitTest {

  @Mock
  private ToggleUserActiveService toggleUserActiveService;

  @InjectMocks
  private ToggleUserActiveControllerImpl toggleUserActiveController;

  @Test
  @DisplayName("Toggle user active")
  void toggleUserActiveTest() {
    User user = helperService.getUser();
    User toggledUser = User.builder()
            .email(user.getEmail())
            .roles(user.getRoles())
            .active(!user.getActive())
            .build();

    when(toggleUserActiveService.toggleUserActive(any()))
            .thenReturn(toggledUser);

    UserDTO result = toggleUserActiveController.toggleUserActive(any());

    assertThat(result.getEmail()).isEqualTo(user.getEmail());
    assertThat(result.getActive()).isEqualTo(!user.getActive());
  }

}
