package com.company.key.unit.controller.role;

import com.company.key.controllers.v1.role.impl.RemoveRoleFromUserControllerImpl;
import com.company.key.dtos.user.UserDTO;
import com.company.key.infrastructure.base.BaseUnitTest;
import com.company.key.models.role.Role;
import com.company.key.models.user.User;
import com.company.key.services.role.RemoveRoleFromUserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class RemoveRoleFromUserControllerTest extends BaseUnitTest {

  @Mock
  private RemoveRoleFromUserService removeRoleFromUserService;

  @InjectMocks
  private RemoveRoleFromUserControllerImpl removeRoleFromUserController;

  @Test
  @DisplayName("Successfully removed role")
  void successTest() {
    Role role = helperService.getAdminRole();
    User user = helperService.getUser();
    User admin = helperService.getAdmin();

    when(removeRoleFromUserService.removeRoleFromUser(role.getId(), user.getId()))
            .thenReturn(user);

    UserDTO result = removeRoleFromUserController.removeRoleFromUser(role.getId(), user.getId());

    assertThat(result.getRoles()).hasSize(1);
  }

}
