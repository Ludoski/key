package com.company.key.infrastructure.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import redis.embedded.RedisServer;

@TestConfiguration
public class EmbeddedRedis {

  @Value("${spring.redis.port}")
  private int port;

  @Bean(initMethod = "start", destroyMethod = "stop")
  public RedisServer embeddedRedisServer() {
    return new RedisServer(port);
  }

}
