package com.company.key.infrastructure.base;

import com.company.key.infrastructure.helpers.UnitHelperService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
public class BaseUnitTest {

  protected UnitHelperService helperService = new UnitHelperService();

}
