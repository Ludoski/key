package com.company.key.infrastructure.base;

import com.company.key.constants.ApiConstants;
import com.company.key.dtos.auth.LoginDTO;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.dtos.response.ResponseBody;
import com.company.key.infrastructure.helpers.IntegrationHelperService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@SpringBootTest
public class BaseIntegrationTest {

  @Autowired
  protected IntegrationHelperService helperService;

  @Autowired
  protected MockMvc mockMvc;

  protected final ObjectMapper objectMapper = new ObjectMapper();

  protected TokensDTO login(String email, String password) throws Exception {
    LoginDTO loginDTO = LoginDTO.builder()
            .email(email)
            .password(password)
            .build();
    ResultActions result
            = mockMvc.perform(
                    post(ApiConstants.V1_LOGIN)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(loginDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail.access_token").isNotEmpty())
            .andExpect(jsonPath("$.detail.refresh_token").isNotEmpty());

    String resultString = result.andReturn().getResponse().getContentAsString();
    ResponseBody responseBody = objectMapper.readValue(resultString, ResponseBody.class);
    return objectMapper.convertValue(responseBody.getDetail(), TokensDTO.class);
  }

}
