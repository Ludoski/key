package com.company.key.infrastructure.helpers;

import com.company.key.dtos.auth.LoginDTO;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.enums.UserRole;
import com.company.key.models.role.Role;
import com.company.key.models.user.User;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Set;
import java.util.UUID;

/**
 * Helper service for unit testing
 */
public class UnitHelperService {

  public String getRandomEmail() {
    return UUID.randomUUID() + "@email.com";
  }

  public String getRandomString() {
    return UUID.randomUUID().toString();
  }

  public LoginDTO getLoginDTO() {
    return LoginDTO.builder()
            .email(getRandomEmail())
            .password(getRandomString())
            .build();
  }

  public TokensDTO getTokens() {
    return TokensDTO.builder()
            .accessToken("access_token")
            .refreshToken("refresh_token")
            .build();
  }

  public User getUser() {
    return User.builder()
            .id(UUID.randomUUID())
            .email(getRandomEmail())
            .password(getRandomString())
            .username(getRandomString())
            .firstName(getRandomString())
            .lastName(getRandomString())
            .roles(Set.of(getUserRole()))
            .active(true)
            .createdAt(Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime()))
            .activationToken(UUID.randomUUID())
            .build();
  }

  public User getAdmin() {
    return User.builder()
            .id(UUID.randomUUID())
            .email(getRandomEmail())
            .password(getRandomString())
            .username(getRandomString())
            .firstName(getRandomString())
            .lastName(getRandomString())
            .roles(Set.of(getUserRole(), getAdminRole()))
            .active(true)
            .createdAt(Timestamp.valueOf(Instant.now().atOffset(ZoneOffset.UTC).toLocalDateTime()))
            .activationToken(UUID.randomUUID())
            .build();
  }

  public Role getUserRole() {
    return Role.builder()
            .id(UUID.randomUUID())
            .slug(UserRole.ROLE_USER)
            .name("user role")
            .description("User role description.")
            .build();
  }

  public Role getAdminRole() {
    return Role.builder()
            .id(UUID.randomUUID())
            .slug(UserRole.ROLE_ADMIN)
            .name("admin role")
            .description("Admin role description.")
            .build();
  }

}
