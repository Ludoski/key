package com.company.key.integration.user;

import com.company.key.constants.ApiConstants;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.infrastructure.base.BaseIntegrationTest;
import com.company.key.models.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ToggleUserActiveTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - User inactive")
  void success1Test() throws Exception {
    String adminEmail = helperService.getRandomEmail();
    String adminPassword = helperService.getRandomString();
    String userEmail = helperService.getRandomEmail();
    String userPassword = helperService.getRandomString();

    // Save valid admin
    helperService.saveAdmin(adminEmail, adminPassword, true);

    // Save valid user
    User user = helperService.saveUser(userEmail, userPassword, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(adminEmail, adminPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_TOGGLE_USER_ACTIVE, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail").isNotEmpty())
            .andExpect(jsonPath("$.detail.active").value(false));
  }

  @Test
  @DisplayName("200 - User activated")
  void success2lTest() throws Exception {
    String adminEmail = helperService.getRandomEmail();
    String adminPassword = helperService.getRandomString();
    String userEmail = helperService.getRandomEmail();
    String userPassword = helperService.getRandomString();

    // Save valid admin
    helperService.saveAdmin(adminEmail, adminPassword, true);

    // Save valid user
    User user = helperService.saveUser(userEmail, userPassword, false);

    // Login and get tokens
    TokensDTO tokensDTO = login(adminEmail, adminPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_TOGGLE_USER_ACTIVE, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail").isNotEmpty())
            .andExpect(jsonPath("$.detail.active").value(true));
  }

  @Test
  @DisplayName("400 - Wrong id format")
  void wrongIdFormatTest() throws Exception {
    String adminEmail = helperService.getRandomEmail();
    String adminPassword = helperService.getRandomString();
    String userEmail = helperService.getRandomEmail();
    String userPassword = helperService.getRandomString();

    // Save valid admin
    helperService.saveAdmin(adminEmail, adminPassword, true);

    // Save valid user
    helperService.saveUser(userEmail, userPassword, false);

    // Login and get tokens
    TokensDTO tokensDTO = login(adminEmail, adminPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_TOGGLE_USER_ACTIVE, 123456)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("401 - Unauthorized")
  void unauthorizedTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Check response without authorization
    mockMvc.perform(
                    patch(ApiConstants.V1_TOGGLE_USER_ACTIVE, user.getId())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnauthorized());
  }

  @Test
  @DisplayName("403 - Fail if not admin")
  void failIfNotAdminTest() throws Exception {
    String email1 = helperService.getRandomEmail();
    String password1 = helperService.getRandomString();
    String email2 = helperService.getRandomEmail();
    String password2 = helperService.getRandomString();

    // Save valid user1
    helperService.saveUser(email1, password1, true);

    // Save valid user2
    User user2 = helperService.saveUser(email2, password2, true);

    // Login and get tokens as user 1
    TokensDTO tokensDTO = login(email1, password1);

    // Check response when trying to get other user and not admin
    mockMvc.perform(
                    patch(ApiConstants.V1_TOGGLE_USER_ACTIVE, user2.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isForbidden());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Try to get non-existent user
    mockMvc.perform(
                    patch(ApiConstants.V1_TOGGLE_USER_ACTIVE, UUID.randomUUID())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String adminEmail = helperService.getRandomEmail();
    String adminPassword = helperService.getRandomString();
    String userEmail = helperService.getRandomEmail();
    String userPassword = helperService.getRandomString();

    // Save valid admin
    helperService.saveAdmin(adminEmail, adminPassword, true);

    // Save valid user
    User user = helperService.saveUser(userEmail, userPassword, false);

    // Login and get tokens
    TokensDTO tokensDTO = login(adminEmail, adminPassword);

    // Check response
    mockMvc.perform(
                    get(ApiConstants.V1_TOGGLE_USER_ACTIVE, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

}
