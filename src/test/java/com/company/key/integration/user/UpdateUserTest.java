package com.company.key.integration.user;

import com.company.key.constants.ApiConstants;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.dtos.user.UpdateUserDTO;
import com.company.key.infrastructure.base.BaseIntegrationTest;
import com.company.key.models.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class UpdateUserTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - User updated as admin")
  void success1Test() throws Exception {
    String adminEmail = helperService.getRandomEmail();
    String adminPassword = helperService.getRandomString();
    String userEmail = helperService.getRandomEmail();
    String userPassword = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(helperService.getRandomEmail())
            .build();

    // Save valid admin
    helperService.saveAdmin(adminEmail, adminPassword, true);

    // Save valid user
    User user = helperService.saveUser(userEmail, userPassword, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(adminEmail, adminPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail").isNotEmpty());
  }

  @Test
  @DisplayName("200 - User update self")
  void success2Test() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(helperService.getRandomEmail())
            .build();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail").isNotEmpty());
  }

  @Test
  @DisplayName("400 - Wrong id format")
  void wrongIdFormatTest() throws Exception {
    String adminEmail = helperService.getRandomEmail();
    String adminPassword = helperService.getRandomString();
    String userEmail = helperService.getRandomEmail();
    String userPassword = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(helperService.getRandomEmail())
            .build();

    // Save valid admin
    helperService.saveAdmin(adminEmail, adminPassword, true);

    // Save valid user
    helperService.saveUser(userEmail, userPassword, false);

    // Login and get tokens
    TokensDTO tokensDTO = login(adminEmail, adminPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, 123456)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Empty body")
  void emptyBodyTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(null))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email wrong format")
  void emailWrongFormatTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email("wrong")
            .build();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email is empty")
  void emailIsEmptyTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email("")
            .build();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email is space")
  void emailIsSpaceTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(" ")
            .build();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("401 - Unauthorized")
  void unauthorizedTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(" ")
            .build();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, user.getId())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnauthorized());
  }

  @Test
  @DisplayName("403 - Fail if not admin")
  void failIfNotAdminTest() throws Exception {
    String email1 = helperService.getRandomEmail();
    String email2 = helperService.getRandomEmail();
    String password1 = helperService.getRandomString();
    String password2 = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(helperService.getRandomEmail())
            .build();

    // Save valid user 1
    helperService.saveUser(email1, password1, true);

    // Save valid user 2
    User user2 = helperService.saveUser(email2, password2, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email1, password1);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, user2.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isForbidden());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(helperService.getRandomEmail())
            .build();

    // Save valid user
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, UUID.randomUUID())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(" ")
            .build();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_UPDATE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

  @Test
  @DisplayName("415 - Invalid content type")
  void invalidContentType() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
            .email(" ")
            .build();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_UPDATE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .content(objectMapper.writeValueAsString(updateUserDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnsupportedMediaType());
  }

}
