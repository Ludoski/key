package com.company.key.integration.user;

import com.company.key.constants.ApiConstants;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.infrastructure.base.BaseIntegrationTest;
import com.company.key.models.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class DeleteUserTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Delete user by id as admin")
  void success1Test() throws Exception {
    String adminEmail = helperService.getRandomEmail();
    String adminPassword = helperService.getRandomString();
    String userEmail = helperService.getRandomEmail();
    String userPassword = helperService.getRandomString();

    // Save valid admin
    helperService.saveAdmin(adminEmail, adminPassword, true);

    // Save valid user
    User user = helperService.saveUser(userEmail, userPassword, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(adminEmail, adminPassword);

    // Check response
    mockMvc.perform(
                    delete(ApiConstants.V1_DELETE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
            )
            .andExpect(status().isOk());
  }

  @Test
  @DisplayName("200 - Delete user by id for self")
  void success2Test() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    delete(ApiConstants.V1_DELETE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
            )
            .andExpect(status().isOk());
  }

  @Test
  @DisplayName("400 - Wrong id format")
  void wrongIdFormatTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    delete(ApiConstants.V1_DELETE_USER, 123456)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("401 - Unauthorized")
  void unauthorizedTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Check response without authorization
    mockMvc.perform(
                    delete(ApiConstants.V1_DELETE_USER, user.getId())
            )
            .andExpect(status().isUnauthorized());
  }

  @Test
  @DisplayName("403 - Try to delete other user by id and not admin")
  void failIfNotAdminTest() throws Exception {
    String email1 = helperService.getRandomEmail();
    String password1 = helperService.getRandomString();
    String email2 = helperService.getRandomEmail();
    String password2 = helperService.getRandomString();

    // Save valid user1
    helperService.saveUser(email1, password1, true);

    // Save valid user2
    User user2 = helperService.saveUser(email2, password2, true);

    // Login and get tokens as user 1
    TokensDTO tokensDTO = login(email1, password1);

    // Check response when trying to delete other user and not admin
    mockMvc.perform(
                    delete(ApiConstants.V1_DELETE_USER, user2.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
            )
            .andExpect(status().isForbidden());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Try to get non-existent user
    mockMvc.perform(
                    delete(ApiConstants.V1_DELETE_USER, UUID.randomUUID())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String adminEmail = helperService.getRandomEmail();
    String adminPassword = helperService.getRandomString();
    String userEmail = helperService.getRandomEmail();
    String userPassword = helperService.getRandomString();

    // Save valid admin
    helperService.saveAdmin(adminEmail, adminPassword, true);

    // Save valid user
    User user = helperService.saveUser(userEmail, userPassword, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(adminEmail, adminPassword);

    // Check response
    mockMvc.perform(
                    put(ApiConstants.V1_DELETE_USER, user.getId())
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
            )
            .andExpect(status().isMethodNotAllowed());
  }

}
