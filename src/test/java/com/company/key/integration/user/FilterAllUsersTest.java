package com.company.key.integration.user;

import com.company.key.constants.ApiConstants;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.dtos.user.FilterUsersDTO;
import com.company.key.infrastructure.base.BaseIntegrationTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class FilterAllUsersTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Filter all users as admin")
  void success1Test() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = "check";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail").isNotEmpty());
  }

  @Test
  @DisplayName("200 - Filter all users as admin, filter null")
  void success2Test() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = null;
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail").isNotEmpty());
  }

  @Test
  @DisplayName("200 - Filter all users as admin, filter empty")
  void success3Test() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = "";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail").isNotEmpty());
  }

  @Test
  @DisplayName("200 - Filter all users as admin, filter space")
  void success4Test() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = " ";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail").isNotEmpty());
  }

  @Test
  @DisplayName("400 - Page param missing")
  void pageParamIsMissingTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = "check";
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .size(size)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Page param is negative")
  void pageParamIsNegativeTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = "check";
    int page = -1;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Size param is missing")
  void sizeParamIsMissingTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = "check";
    int page = 0;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Size param is negative")
  void sizeParamIsNegativeTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = "check";
    int page = 0;
    int size = -1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("401 - Unauthorized")
  void unauthorizedTest() throws Exception {
    String filter = "check";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnauthorized());
  }

  @Test
  @DisplayName("403 - Fail if not admin")
  void failIfNotAdminTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = "check";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isForbidden());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = "check";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    put(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

  @Test
  @DisplayName("415 - Invalid content type")
  void invalidContentTypeTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();
    String filter = "check";
    int page = 0;
    int size = 1;
    FilterUsersDTO filterUsersDTO = FilterUsersDTO.builder()
            .filter(filter)
            .page(page)
            .size(size)
            .build();

    // Save valid admin
    helperService.saveAdmin(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_FILTER_ALL_USERS)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .content(objectMapper.writeValueAsString(filterUsersDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnsupportedMediaType());
  }


}
