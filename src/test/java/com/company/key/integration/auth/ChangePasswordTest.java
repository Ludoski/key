package com.company.key.integration.auth;

import com.company.key.constants.ApiConstants;
import com.company.key.dtos.auth.ChangePasswordDTO;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.infrastructure.base.BaseIntegrationTest;
import com.company.key.models.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ChangePasswordTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Password changed")
  void success1Test() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(oldPassword)
            .newPassword(newPassword)
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()));
  }

  @Test
  @DisplayName("400 - Empty body")
  void emptyBodyTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(null))
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No old password in request")
  void noOldPasswordInRequestTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .newPassword(newPassword)
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No new password in request")
  void noNewPasswordInRequestTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(newPassword)
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Old password is empty")
  void oldPasswordIsEmptyTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword("")
            .newPassword(newPassword)
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - New password is empty")
  void newPasswordIsEmptyTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(oldPassword)
            .newPassword("")
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Old password is space")
  void oldPasswordIsSpaceTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(" ")
            .newPassword(newPassword)
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - New password is space")
  void newPasswordIsSpaceTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(oldPassword)
            .newPassword(" ")
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("401 - Unauthorized")
  void unauthorizedTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(oldPassword)
            .newPassword(newPassword)
            .build();

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isUnauthorized());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    User user = helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(oldPassword)
            .newPassword(newPassword)
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Delete user
    helperService.deleteUser(user);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(oldPassword)
            .newPassword(newPassword)
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isMethodNotAllowed());
  }

  @Test
  @DisplayName("415 - Invalid content type")
  void invalidContentType() throws Exception {
    String email = helperService.getRandomEmail();
    String oldPassword = helperService.getRandomString();
    String newPassword = helperService.getRandomString();
    helperService.saveUser(email, oldPassword, true);
    ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
            .oldPassword(oldPassword)
            .newPassword(newPassword)
            .build();

    // Login and get tokens
    TokensDTO tokensDTO = login(email, oldPassword);

    // Check response
    mockMvc.perform(
                    patch(ApiConstants.V1_CHANGE_PASSWORD)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .content(objectMapper.writeValueAsString(changePasswordDTO))
            )
            .andExpect(status().isUnsupportedMediaType());
  }

}
