package com.company.key.integration.auth;

import com.company.key.constants.ApiConstants;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.infrastructure.base.BaseIntegrationTest;
import com.company.key.models.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class RefreshTokenTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - New access token generated")
  void successTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Try to refresh tokens with valid authorization
    mockMvc.perform(
                    get(ApiConstants.V1_REFRESH_TOKEN)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail.access_token").isNotEmpty());
  }

  @Test
  @DisplayName("401 - Unauthorized")
  void unauthorizedTest() throws Exception {
    // Try to refresh tokens with invalid authorization
    mockMvc.perform(
                    get(ApiConstants.V1_REFRESH_TOKEN)
                            .header("Authorization", "Bearer " + "invalid token")
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnauthorized());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Delete user
    helperService.deleteUser(user);

    // Try to refresh tokens with valid authorization but deleted user
    mockMvc.perform(
                    get(ApiConstants.V1_REFRESH_TOKEN)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login and get tokens
    TokensDTO tokensDTO = login(email, password);

    // Try to refresh tokens with valid authorization
    mockMvc.perform(
                    post(ApiConstants.V1_REFRESH_TOKEN)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

}
