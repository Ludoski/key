package com.company.key.integration.auth;

import com.company.key.constants.ApiConstants;
import com.company.key.dtos.auth.PasswordResetInitiateDTO;
import com.company.key.dtos.auth.TokensDTO;
import com.company.key.infrastructure.base.BaseIntegrationTest;
import com.company.key.models.user.User;
import com.company.key.services.email.PasswordResetEmailService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PasswordResetInitiateTest extends BaseIntegrationTest {

  @MockBean
  private PasswordResetEmailService passwordResetEmailService;

  @Test
  @DisplayName("200 - Password reset initiated")
  void successTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    // Prepare valid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    // Prevent email sending
    doNothing().when(passwordResetEmailService).sendPasswordResetEmail(any(), any());

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.detail").isEmpty());
  }

  @Test
  @DisplayName("400 - Request body is empty")
  void requestBodyIsEmptyTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(null))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - No email in request")
  void noEmailInRequestTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    // Prepare invalid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(null)
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Wrong email format")
  void wrongEmailFormatTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    // Prepare invalid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email("email")
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email empty")
  void emailEmptyTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email("")
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Email space")
  void emailSpaceTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(" ")
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("404 - Not found")
  void notFoundTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email("test@test.com")
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNotFound());
  }

  @Test
  @DisplayName("405 - Wrong HTTP method")
  void wrongHttpMethodTest() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    // Prepare valid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    // Check response
    mockMvc.perform(
                    delete(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isMethodNotAllowed());
  }

  @Test
  @DisplayName("415 - Invalid content type")
  void invalidContentType() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    // Prepare valid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isUnsupportedMediaType());
  }

  // TODO Enable this when email activation is configured
  /*@Test
  @DisplayName("500 - Unable to send email")
  void unableToSendEmail() throws Exception {
    String email = helperService.getRandomEmail();
    String password = helperService.getRandomString();

    // Save valid user
    User user = helperService.saveUser(email, password, true);

    // Login
    TokensDTO tokensDTO = login(email, password);

    // Prepare valid payload
    PasswordResetInitiateDTO passwordResetInitiateDTO = PasswordResetInitiateDTO.builder()
            .email(user.getEmail())
            .build();

    doThrow(MailAuthenticationException.class)
            .when(passwordResetEmailService).sendPasswordResetEmail(any(), any());

    // Check response
    mockMvc.perform(
                    post(ApiConstants.V1_PASSWORD_RESET)
                            .header("Authorization", "Bearer " + tokensDTO.getAccessToken())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(objectMapper.writeValueAsString(passwordResetInitiateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isInternalServerError());
  }*/

}
